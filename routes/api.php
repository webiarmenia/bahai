<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('webi')->namespace('API')->group(function () {
    // Menus
//    Route::get('/menus','MenusController@getMenus');
    Route::get('/getHeaderMenu','MenusController@getHeaderMenu');
    Route::get('/getFooterMenu','MenusController@getFooterMenu');
    // Pagination
    Route::post('/pagination','PagesController@pagination');
    // PagesId
//    Route::get('/getPageId','MenusController@getPageId');
    Route::get('/get-page/{slug}','MenusController@getPageId');
    // Pages
    Route::get('/getPages','PagesController@getPages');
    // Subscriber
    Route::post('/createSubscriber','SubscriberControllerApi@postCreate');
    // Create
//    Route::post('/create','LoginController@create');
//    // Login
//    Route::post('/login','LoginController@login');

    //News
    Route::get('/search','NewsController@getSearchResults');

    //newsId
    Route::get('/getNews','NewsController@newsId');

    //Translation
    Route::get('/getlabels','PagesController@getLabels');

    //Slider
    Route::get('/getSlider','PagesController@getBanner');

    //pages
//    Route::get('/getPostsByCategory','PostsController@getPostsByCategory');
//    Route::get('/getCategory','PostsController@getCategory');

    //links
    Route::get('/getAllLinks','MenusController@getAllLinks');
    Route::get('/getLink','MenusController@getLink');

//    Route::get('/get-page/{}','MenusController@getPage');

    //Home Data
    Route::get('/getHomeData','MenusController@getHomeData');

    Route::get('/get-settings','MenusController@getSetting');

    //Section
    Route::get('/getSection','MenusController@getSection');

    //Banner(in bahai admin Sections)
    Route::get('/getBanners','MenusController@getBanners');
    Route::get('/get-data','MenusController@getData');
    Route::get('/get-home-content','MenusController@getHomeContent');

    Route::post('/contactUs','MenusController@contactUs');
});
