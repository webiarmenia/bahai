<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
   dd('apa');
});


Route::get('locale/{locale}', function ($locale) {
    \Session::put('locale', $locale);
    return redirect()->back();
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function () {
    return view('bahai.index');
});
Route::get('/admin',function () {
    if (Auth::user()){
        return redirect()->route('admin.subscribers');
    }else{
        return view('admin.index');
    }
})->name('admin');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
});

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('admin')->group(function () {

        //subscribers
        Route::get('/subscribers','SubscribersController@index')->name('admin.subscribers');
        Route::post('/subscribers/subscriberChange/{id}','SubscribersController@subscriberChange')->name('admin.subscriberChange');
        Route::post('/subscribers/subscriberDestroy/{id}','SubscribersController@subscriberDestroy')->name('admin.subscriberDestroy');
        Route::post('subscribers/delete', 'SubscribersController@deleteAll')->name('admin.subscribers.deleteAll');

        //mailing
        Route::get('/mailing','MailingController@index')->name('admin.mailing');
        Route::get('/mailing/create','MailingController@create')->name('admin.mailing.create');
        Route::get('/mailing/edit/{id}','MailingController@edit')->name('admin.mailing.edit');
        Route::get('/mailing/show/{id}','MailingController@show')->name('admin.mailing.show');
        Route::post('/mailing/destroy/{id}','MailingController@destroy')->name('admin.mailing.destroy');
        Route::post('/mailing/sendEmail','MailingController@sendEmail')->name('admin.sendEmail');
        Route::post('/mailing/updateEmail/{id}','MailingController@updateEmail')->name('admin.updateEmail');
        Route::post('mailing/delete', 'MailingController@deleteAll')->name('admin.mailing.deleteAll');

        //news
        Route::get('news','NewsController@index')->name('admin.news');
        Route::get('news/create','NewsController@create')->name('admin.news.create');
        Route::post('news/addNews','NewsController@addNews')->name('admin.addNews');
        Route::post('news/updateNews/{id}','NewsController@updateNews')->name('admin.updateNews');
        Route::get('news/show/{id}','NewsController@show')->name('admin.news.show');
        Route::get('news/edit/{id}','NewsController@edit')->name('admin.news.edit');
        Route::post('news/destroy/{id}','NewsController@destroy')->name('admin.news.destroy');
        Route::post('news/delete', 'NewsController@deleteAll')->name('admin.news.deleteAll');

        //sections
        Route::get('sections','SectionsController@index')->name('admin.sections');
        Route::get('sections/create','SectionsController@create')->name('admin.sections.create');
        Route::post('sections/store','SectionsController@store')->name('admin.sections.store');
        Route::get('sections/edit/{id}','SectionsController@edit')->name('admin.sections.edit');
        Route::post('section/update/{id}','SectionsController@update')->name('admin.sections.update');
        Route::get('section/show/{id}','SectionsController@show')->name('admin.sections.show');
        Route::post('section/destroy/{id}','SectionsController@destroy')->name('admin.sections.destroy');
        Route::post('sections/delete', 'SectionsController@deleteAll')->name('admin.section.deleteAll');

        //settings
        Route::get('settings','SettingsController@index')->name('admin.settings');
        Route::get('settings/create','SettingsController@create')->name('admin.settings.create');
        Route::post('settings/store','SettingsController@store')->name('admin.settings.store');
        Route::get('settings/edit/{id}','SettingsController@edit')->name('admin.settings.edit');
        Route::post('settings/update/{id}','SettingsController@update')->name('admin.settings.update');
        Route::get('settings/show/{id}','SettingsController@show')->name('admin.settings.show');
        Route::post('settings/destroy/{id}','SettingsController@destroy')->name('admin.settings.destroy');
        Route::post('settings/delete', 'SettingsController@deleteAll')->name('admin.settings.deleteAll');

        //menus
        Route::get('menus', 'MenusController@index')->name('admin.menus');
        Route::get('menus/create', 'MenusController@create')->name('admin.menus.create');
        Route::post('menus/store', 'MenusController@store')->name('admin.menus.store');
        Route::post('menus/destroy/{id}','MenusController@Destroy')->name('admin.menus.destroy');
        Route::get('menus/edit/{id}','MenusController@edit')->name('admin.menus.edit');
        Route::post('menus/update/{id}','MenusController@update')->name('admin.menus.update');
        Route::post('menus/delete', 'MenusController@deleteAll')->name('admin.menus.deleteAll');

        //pages
        Route::get('pages','PageController@index')->name('admin.pages');
        Route::post('pages', 'PageController@store')->name('admin.pages.store');
        Route::get('pages/create', 'PageController@create')->name('admin.pages.create');
        Route::get('pages/update/{id}', 'PageController@edit')->name('admin.pages.edit');
        Route::post('pages/update/{id}', 'PageController@update')->name('admin.pages.update');
        Route::post('pages/destroy/{id}', 'PageController@destroy')->name('admin.pages.destroy');
        Route::post('pages/delete', 'PageController@deleteAll')->name('admin.pages.deleteAll');

        //links
        Route::get('links','LinksController@index')->name('admin.links');
        Route::post('links', 'LinksController@store')->name('admin.links.store');
        Route::get('links/create', 'LinksController@create')->name('admin.links.create');
        Route::get('links/update/{id}', 'LinksController@edit')->name('admin.links.edit');
        Route::post('links/update/{id}', 'LinksController@update')->name('admin.links.update');
        Route::post('links/destroy/{id}', 'LinksController@destroy')->name('admin.links.destroy');
        Route::post('links/delete', 'LinksController@deleteAll')->name('admin.links.deleteAll');

        //translations
        Route::get('translations','TranslationsController@index')->name('admin.translations');
        Route::post('translations', 'TranslationsController@store')->name('admin.translations.store');
        Route::get('translations/create', 'TranslationsController@create')->name('admin.translations.create');
        Route::get('translations/update/{id}', 'TranslationsController@edit')->name('admin.translations.edit');
        Route::post('translations/update/{id}', 'TranslationsController@update')->name('admin.translations.update');
        Route::post('translations/destroy/{id}', 'TranslationsController@destroy')->name('admin.translations.destroy');
        Route::post('translations/delete', 'TranslationsController@deleteAll')->name('admin.translations.deleteAll');

        //sliders
        Route::get('sliders','SlidersController@index')->name('admin.sliders');
        Route::post('sliders', 'SlidersController@store')->name('admin.sliders.store');
        Route::get('sliders/create', 'SlidersController@create')->name('admin.sliders.create');
        Route::get('sliders/update/{id}', 'SlidersController@edit')->name('admin.sliders.edit');
        Route::post('sliders/update/{id}', 'SlidersController@update')->name('admin.sliders.update');
        Route::post('sliders/destroy/{id}', 'SlidersController@destroy')->name('admin.sliders.destroy');
        Route::post('sliders/delete', 'SlidersController@deleteAll')->name('admin.sliders.deleteAll');

        //categories
        Route::get('categories','CategoriesController@index')->name('admin.categories');
        Route::get('category/create','CategoriesController@create')->name('admin.categories.create');
        Route::post('category/store','CategoriesController@store')->name('admin.categories.store');
        Route::get('category/edit/{id}','CategoriesController@edit')->name('admin.categories.edit');
        Route::post('category/update/{id}','CategoriesController@update')->name('admin.categories.update');
        Route::post('category/destroy/{id}','CategoriesController@destroy')->name('admin.categories.destroy');
        Route::post('categories/delete', 'CategoriesController@deleteAll')->name('admin.categories.deleteAll');
        //categories
        Route::get('posts','PostsController@index')->name('admin.posts');
        Route::get('post/create','PostsController@create')->name('admin.posts.create');
        Route::post('post/store','PostsController@store')->name('admin.posts.store');
        Route::get('post/edit/{id}','PostsController@edit')->name('admin.posts.edit');
        Route::post('post/update/{id}','PostsController@update')->name('admin.posts.update');
        Route::post('post/destroy/{id}','PostsController@destroy')->name('admin.posts.destroy');
        Route::post('posts/delete', 'PostsController@deleteAll')->name('admin.posts.deleteAll');

    });
});
