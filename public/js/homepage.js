$(document).ready(function () {
    $('.custom-navbar-nav li').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $("." + $(this).attr('id') + '-content').removeClass('submenu_block');
        } else {
            $('.custom-navbar-nav li').each(function () {
                $(this).removeClass('active');
                $("." + $(this).attr('id') + '-content').removeClass('submenu_block');
            });
            $(this).addClass('active');
            $("." + $(this).attr('id') + '-content').addClass('submenu_block');

            if ($('.active .caret-down').hasClass('down')) {
                $('.active .caret-down').removeClass('down');
            } else {
                $('.active .caret-down').each(function () {
                    $('.active .caret-down').removeClass('down');
                });
            }
            $(".hidden-menu-open ul li:first-child").addClass('active');
            $('.hidden-menu-open p:first-child').addClass('active');
            if ($('.hidden-menu-open ul li:first-child').hasClass('active')) {
                $('.hidden-menu-open ul li:first-child').addClass('hidden-menu-border');
                $('.hidden-menu-open p:first-child').addClass('hidden-menu-block');
            }
        }
    });
    $(window).click(function (e) {
        if (e.target.className != 'hidden-menu-border' && e.target.className != 'menu') {
            if ($('.submenu.submenu_block')) {
                $('.submenu.submenu_block').removeClass('submenu_block');
                if ($('.active .caret-down').hasClass('down')) {
                    $('.active .caret-down').removeClass('down');
                }
                $('.active .caret-down').addClass('down');
            }
        }
    });
    $('.list-items li').click(function () {
        mnuitemHover(this);
    });
    var delay = 400, setTimeoutConst;
    $('.list-items li').hover(function () {
        setTimeoutConst = setTimeout(function () {
            mnuitemHover(this);
        }.bind(this), delay);
    }, function () {
        clearTimeout(setTimeoutConst);
    });

    function mnuitemHover(_this) {
        $('.list-items li').removeClass('hidden-menu-border');
        $('.hidden-menu-info p').removeClass('hidden-menu-block');
        $(_this).addClass("hidden-menu-border");
        $('.' + $(_this).attr('id') + '-content').addClass("hidden-menu-block");
    }

    $('.third-button').on('click', function () {
 $('.collapse:not(.show)').t;
        if ($('.animated-icon2').hasClass('open')) {
            $('.menu-search-hidden').css('display', 'none');
            $('.animated-icon2').css('display', 'none');
            $('.btn-search-hidden').css('display', 'block');
            $('.animated-icon2').toggleClass('open');
        }
        $('.list-items li').hover(function () {
            $('.list-items li').removeClass('hidden-menu-border');
            $('.hidden-menu-info p').removeClass('hidden-menu-block');
            $('.' + $('.list-items li').attr('id') + '-content').removeClass("hidden-menu-block");
        });
        $('.custom-navbar-nav li').on('click', function () {
            $(".hidden-menu-open ul li:first-child").removeClass('active');
            $('.hidden-menu-open p:first-child').removeClass('active');
            if ($('.hidden-menu-open ul li:first-child').hasClass('active')) {
                $('.hidden-menu-open ul li:first-child').removeClass('hidden-menu-border');
                $('.hidden-menu-open p:first-child').removeClass('hidden-menu-block');
            }
        });
        $('.animated-icon3').toggleClass('open');
        $('.collapse').toggleClass('show');
        $("." + $(this).attr('id') + '-content').removeClass('submenu_block');
    });
    $('.btn-search-hidden ').on('click', function () {
        if ($('.animated-icon3').hasClass('open')) {
            $('#menu-opener').click();
        }
        $('.btn-search-hidden ').css('display', 'none');
        $('.animated-icon2').css('display', 'block');
        $('.menu-search-hidden').css('display', 'block');
        $('.animated-icon2').toggleClass('open');
    });

    $('.animated-icon2').on('click', function () {
        $('.btn-search-hidden ').css('display', 'block');
        $('.animated-icon2').css('display', 'none');
        $('.menu-search-hidden').css('display', 'none');
        $('.animated-icon2').toggleClass('open');
    });

    $('.menu-search-hidden li').on('click', function () {
        if ($('.menu-search-hidden li span').hasClass('open-block')) {
            $('.menu-search-hidden li span').removeClass('open-block');
            $('.menu-search-hidden li span').next().removeClass('color');
        }
        $(this).find('span').addClass('open-block');
        $(this).find('span').next().addClass('color');
    });
    $(".menu-search-hidden ul li:first-child  span").addClass('open-block');
    $(".menu-search-hidden ul li:first-child  span").next().addClass('color');


    $('.languages').on('click', function () {
        $('.lang').addClass('lang-block');
    })

});