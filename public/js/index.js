$(document).ready(function () {
    $('.menu').on('click', function (e) {

        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find(".submenu").removeClass('submenu_block');
        } else {
            $('.menu').each(function () {
                $(this).removeClass('active');
                $(this).find(".submenu").removeClass('submenu_block');
            });
            $(this).addClass('active');
            $(this).find(".submenu").addClass('submenu_block');


            $(".menu ul li:first-child").addClass('active');
            $('.hidden-menu-info p:first-child').addClass('active');

            if ($('.menu ul li:first-child').hasClass('active')) {
                $('.menu ul li:first-child').addClass('hidden-menu-border')
                $('.hidden-menu-info p:first-child').addClass('hidden-menu-block')
            }



        }
    });

    $('.list-items li').hover(showMenu)
    $('.list-items li').click(showMenu)

    function showMenu(){
        $('.list-items li').removeClass('hidden-menu-border');
        $('.hidden-menu-info p').removeClass('hidden-menu-block');
        $(this).addClass("hidden-menu-border");
        $('.'+$(this).attr('id')+'-content').addClass("hidden-menu-block");
    }

    $('.third-button').on('click', function () {
        if ($('.animated-icon2').hasClass('open')) {
            $('.menu-search-hidden').css('display', 'none');
            $('.animated-icon2').css('display', 'none');
            $('.btn-search').css('display', 'block');
            $('.animated-icon2').toggleClass('open');
        }
        $('.animated-icon3').toggleClass('open');
    });

    $('.btn-search').on('click', function () {
        if ($('.animated-icon3').hasClass('open')) {
            $('#menu-opener').click();
        }
        $('.btn-search').css('display', 'none');
        $('.animated-icon2').css('display', 'block');
        $('.menu-search-hidden').css('display', 'block');
        $('.animated-icon2').toggleClass('open');
    });

    $('.animated-icon2').on('click', function () {
        $('.btn-search').css('display', 'block');
        $('.animated-icon2').css('display', 'none');
        $('.menu-search-hidden').css('display', 'none');
        $('.animated-icon2').toggleClass('open');
    });

});


