<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">
    <title>Bahai</title>
    <link rel="icon"  href="{{ asset('image/favicon.ico') }}">
    <link href="{{ asset('css/font-face.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
    <link href='{{ asset('fontawesome/css/all.css') }}' type='text/css' rel='stylesheet'>
    <link href="{{ asset('css/theme.css') }}" rel="stylesheet" media="all">
</head>
<body class="animsition">
<div class="page-wrapper">
    <div class="page-content--bge5">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</div>
<script src="{{ asset('js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-progressbar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
</body>
</html>
