<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="au theme template">
        <meta name="author" content="Hau Nguyen">
        <meta name="keywords" content="au theme template">
        <title>Bahai</title>
        <link rel="icon" href="{{ asset('image/favicon.ico') }}">
        <link href="{{ asset('css/material-design-iconic-font.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('css/font-face.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
        <link href='{{ asset('fontawesome/css/all.css') }}' type='text/css' rel='stylesheet'>
        <link href="{{ asset('css/theme.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('css/stylebahai.css') }}" rel="stylesheet" media="all">
        @yield('css')
    </head>
    <body>
        <div class="dashboard-main-wrapper">
            <div class="dashboard-header">
                <nav class="navbar navbar-expand-lg bg-white fixed-top">
                    <a class="" href="{{ url('admin') }}">
                        <img style="width: 17%;" src="{{ asset('image/478886548.png') }}" alt="bahai"/><span
                            style="color: #e6d194;font-size: 19px; padding-left: 20px;">Bahai Admin</span>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <form action="{{ route('logout') }}" method="POST" style="width: 100%;">
                        @csrf
                        <div class="collapse navbar-collapse " id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto navbar-right-top">
                                <li class="nav-item dropdown nav-user">
                                    <a class="nav-link nav-user-img" href="" id="navbarDropdownMenuLink2" data-toggle="dropdown"
                                       aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-user user-avatar-md rounded-circle"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right nav-user-dropdown"
                                         aria-labelledby="navbarDropdownMenuLink2">
                                        <button type="submit" class="dropdown-item"><i class="fas fa-power-off mr-2"></i>Logout
                                        </button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </form>
                </nav>
                <div class="nav-left-sidebar sidebar-dark">
                    <div class="menu-list">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <a class="d-xl-none d-lg-none" href="#">bahai admin</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.categories') }}"> <i class="fa fa-list"></i>Categories</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.sliders') }}"> <i class="fa fa-images"></i>Sliders</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin.translations') }}"><i
                                                class="fa fa-language"></i>Translations</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin.links') }}"> <i
                                                class="fa fa-link"></i>Links</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin.pages') }}"> <i
                                                class="fa fa-file"></i>Pages</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin.subscribers') }}"><i class="fa fa-users"></i>Subscribers</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin.mailing') }}"><i class="fa fa-envelope"></i>Mailing</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin.news') }}"><i class="fa fa-info"></i>News</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin.sections') }}"><i
                                                class="fa fa-puzzle-piece"></i>Sections</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin.settings') }}"><i class="fa fa-cog"></i>Settings</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.menus') }}"><i class="fa fa-bars"></i>
                                            Menus</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="dashboard-wrapper">
                    <div class="dashboard-ecommerce">
                        <div class="container-fluid dashboard-content ">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class=" au-card--no-shadow au-card--no-pad m-b-40">
                                        <main class="py-4">
                                            @yield('content')
                                        </main>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/bootstrap-progressbar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
        @yield('javascript')
    </body>
</html>
