@extends('layouts.login')
@section('content')
    <div class="container">
        <div class="login-wrap">
            <div class="login-content">
                <div class="login-logo"><a href="#"><img class="bahaiadmin" src="/storage/image/admin.png" alt="bahai admin" /></a></div>
                    <div class="login-form">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <div class="form-group">
                                <label>Email Address</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                            <button class="au-btn au-btn--green m-b-20 btn-submit" type="submit">{{ __('Send Password Reset Link') }} </button>
                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection
