<!DOCTYPE html>
<html>
<head>
    <title>Send Email</title>
</head>
<body>
<h1>{{$e_subject}}</h1>
<hr>
{!! $e_message !!}
<hr>
@if($e_link != null)
<a href="{{$e_link}}" >Read More</a>
<hr>
@endif
</body>
</html>