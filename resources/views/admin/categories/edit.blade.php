@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mb-5 offset-xl-1">
            <div class="section-block">
                <h5 class="section-title">Edit Category : {{$item->title_en}}</h5>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="tab-regular">
                <ul class="nav nav-tabs " id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"> Armenia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">English</a>
                    </li>
                </ul>
                <form method="POST" action="{{ route('admin.categories.update', $item->id) }}" enctype="multipart/form-data" >
                    @csrf
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <label for="title_am" class="col-form-label">Title Am <span class="asterisk">*</span></label>
                            <input type="text" name="title_am" id="title_am" class="form-control" value="{{old('title_am')? old('title_am'): $item->title_am}}">
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <label for="title_en" class="col-form-label">Title En </label>
                            <input type="text" name="title_en" id="title_en" class="form-control" value="{{old('title_en')? old('title_en'): $item->title_en}}">
                        </div>

                        <label for="parent_id" class="col-form-label">Parent Id </label>
                        <select name="parent_id" id="parent_id" class="form-control">
                            <option value="{{null}}">No Parent</option>
                            @if($categories->count())
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" @if($item->parent_id == $category->id) {{'selected'}} @endif>{{$category->title_en}}</option>
                                @endforeach
                            @endif
                        </select>

                        <label for="cover" class="col-form-label">Image</label><br/>
                        <input type="file" name="cover" id="cover"><br/>
                        @if($item->cover != null)
                        <input type="hidden" name="coverImage" value="{{$item->cover}}"><br/>
                        <img src="{{asset("storage/".$item->cover . "_large" . "." . $item->ext)}}" width="300px" height="300px"><br/>
                        @endif
                        <br/>
                        <button type="submit" name="create" class="btn btn-success  btn-sm my-0">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.config.autoParagraph = false;
        CKEDITOR.config.filebrowserBrowseUrl  = '/laravel-filemanager';
        CKEDITOR.config.filebrowserUploadUrl   = '/laravel-filemanager/upload';
        CKEDITOR.replace( 'description_en');
        CKEDITOR.replace( 'description_am');
    </script>
@endsection
