@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mb-5 offset-xl-1">
            <div class="section-block">
                <h5 class="section-title">Create Setting</h5>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="tab-regular">
                <ul class="nav nav-tabs " id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">English</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Armenia</a>
                    </li>
                </ul>
                    <form action="{{route('admin.settings.store')}}" method="POST" >
                    @csrf
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <label for="value_en" class="col-form-label">Value En <span class="asterisk">*</span></label>
                            <input  name="value_en" id="value_en" class="form-control"  value="{{old('value_en')}}">
                        </div>

                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <label for="value_en" class="col-form-label">Value Am <span class="asterisk">*</span></label>
                            <input  name="value_am" class="form-control" id="value_am"  value="{{old('value_am')}}">
                        </div>

                        <label for="key" class="col-form-label">Key <span class="asterisk">*</span></label>
                        <input type="text" name="key" class="form-control" id="key" value="{{old('key')}}"><br/>

                        <button type="submit" name="create" class="btn btn-success  btn-sm my-0">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
