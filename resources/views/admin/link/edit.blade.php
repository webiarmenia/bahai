@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mb-5 offset-xl-1">
            <div class="section-block">
                <h5 class="section-title">Edit Link</h5>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="tab-regular">
                <ul class="nav nav-tabs " id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">English</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Armenia</a>
                    </li>
                </ul>
                <form method="post" action="{{ route('admin.links.update', $links->id) }}" enctype="multipart/form-data" >
                    @csrf
                    <div class="tab-content" id="myTabContent">
                        <label for="link" class="col-form-label">Link <span class="asterisk">*</span></label>
                        <input type="text" name="link" id="link" class="form-control"  value="{{old('link')? old('link'):$links->link}}">

                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <label for="text_en" class="col-form-label">Text En</label>
                            <textarea  name="text_en" id="text_en" class="form-control" >{{old('text_en')? old('text_en'):$links->text_en}}</textarea>
                        </div>

                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <label for="text_am" class="col-form-label">Text Am</label>
                            <textarea  name="text_am" id="text_am" class="form-control" >{{old('text_am')? old('text_am'):$links->text_am}}</textarea>
                        </div>


                        <label for="page_id" class="col-form-label">Page Id</label>
                        <select name="page_id" id="page_id" class="form-control">
                            @if($links->page_id == 0)
                                <option value="0" selected >not page</option>
                            @else
                                <option value="0" >not page</option>
                            @endif
                            <option value="home" @if($links->page_id == 'home') selected @endif>Home Page</option>
                            @foreach ($page_id as  $value)
                                <option value="{{ $value->id }}" {{ ( $value->id == $links->page_id)  ? 'selected' : ''}}>{{ $value->title_en }}</option>
                            @endforeach
                        </select>
                        <label for="order" class="col-form-label">Order <span class="asterisk">*</span></label>
                        <input type="number" min="1" name="order" id="order" class="form-control"  value="{{ $links->order}}">

                        <label for="image" class="col-form-label image">Cover</label>
                        <input type="file"  name="image" id="image" value="{{ $links->image}}"><br/><br/>
                        @if($links->image != null)
                            <img src="{{asset("storage/".$links->image . "_large" . "." . $links->ext)}}" width="300px" height="300px"><br/>
                        @endif
                        <br/>

                        <button type="submit" name="create" class="btn btn-success  btn-sm my-0">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
