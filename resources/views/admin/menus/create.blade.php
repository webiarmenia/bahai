@extends('layouts.app')
@section('css')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" media="all">
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mb-5 offset-xl-1">
            <div class="section-block">
                <h5 class="section-title">Create Menu</h5>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="tab-regular">
                <ul class="nav nav-tabs " id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Armenia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">English</a>
                    </li>
                </ul>
                <form method="post" action="{{ route('admin.menus.store') }}"  enctype="multipart/form-data">
                    @csrf
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <label for="title_en" class="col-form-label">Title En </label>
                            <input type="text" name="title_en" id="title_en" class="form-control" value="{{ old('title_en')}}"><br/>

                            <label for="description_en" class="col-form-label">Description En </label>
                            <textarea name="description_en" id="description_en" class="form-control">{{old('description_en')}}</textarea><br/>
                        </div>
                        <div class="tab-pane fade  show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <label for="title_am" class="col-form-label">Title Am <span class="asterisk">*</span></label>
                            <input type="text" name="title_am" id="title_am" class="form-control" value="{{ old('title_am')}}"><br/>

                            <label for="description_am" class="col-form-label">Description Am <span class="asterisk">*</span></label>
                            <textarea name="description_am" id="description_am" class="form-control">{{old('description_am')}}</textarea><br/>
                        </div>

{{--                        <label for="parent" class="col-form-label">Parent Id </label><br/>--}}
{{--                        <select class="js-example-basic-multiple" name="categories[]" multiple="multiple" style="width: 100%!important;">--}}
{{--                            @foreach($categories as $category)--}}
{{--                                <option value="{{$category->id}}">{{$category->title_en}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select><br/>--}}
                        <div class="form-group">
                            <label>Page Id </label>
                            <select name="page_id" class="form-control">
                                <option value="0">No page</option>
                                @foreach($pages as $page)
                                    <option value="{{$page->id}}" {{ old('page_id') == $page->id ? 'selected' : '' }}>{{$page->title_am}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Parent Id <span class="asterisk">*</span></label>
                            <select name="parent_id" class="form-control">
                                <option value="0">No parent</option>
                                @foreach($menus as $menu)
                                    <option value="{{$menu->id}}" {{ old('parent_id') == $menu->id ? 'selected' : '' }}>{{$menu->title_am}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class ='slug'>
                            <label for="slug" class="col-form-label">Slug </label>
                            <input type="text" name="slug" id="slug"class="form-control" value="{{old('slug')}}"><br/>
                        </div>

                        <div class ="url" style="display: none">
                            <label for="url" class="col-form-label">Url</label>
                            <input type="text" name="url" id="ur"class="form-control" value="{{old('url')}}"><br/>
                        </div>

                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="url_enable" name="url_enable"  >
                            <label class="form-check-label " for="url_enable">Active</label>
                        </div>

                        <label for="order" class="col-form-label">Order <span class="asterisk">*</span></label>
                        <input type="number" name="order" id="order"class="form-control" value="{{old('order')}}"><br/>

                        <label>Menus_type <span class="asterisk">*</span></label>
                        <select name="type" class="form-control">
                            <option disabled="disabled" selected="selected">Menus type</option>
                            <option value="1" {{ old('type') == '1' ? 'selected' : '' }}>Header menu</option>
                            <option value="2" {{ old('type') == '2' ? 'selected' : '' }}>Footer menu</option>
                            <option value="3" {{ old('type') == '3' ? 'selected' : '' }}>Footer bottom menu</option>
                        </select><br/>

                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="materialUnchecked" name="active" >
                            <label class="form-check-label" for="materialUnchecked">Active</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="homepage_show" name="homepage_show" >
                            <label class="form-check-label" for="homepage_show">Show in homepage</label>
                        </div>

                        <label for="cover" class="col-form-label image">Image </label>
                        <input type="file" name="cover" id="cover"><br/><br/>

                        <button type="submit" name="create" class="btn btn-success  btn-sm my-0">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="/js/select2.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>
    <script>
        CKEDITOR.config.autoParagraph = false;
        CKEDITOR.config.filebrowserBrowseUrl  = '/laravel-filemanager';
        CKEDITOR.replace( 'description_en');
        CKEDITOR.replace( 'description_am');
    </script>
@endsection
