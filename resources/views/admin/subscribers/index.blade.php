@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-5 m-b-35">Subscribers</h3>
                <div class="table-data__tool">
                    <div class="table-data__tool-left">
                        <div class="rs-select2--dark rs-select2--sm btn-delete">
                            <button class="au-btn au-btn-load delete" data-href="subscribers/delete" onclick="deleteData()">Delete</button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive table--no-card m-b-30">
                    <table class="table table-borderless table-striped table-earning">
                        <thead class="">
                        <tr>
                            <th>
                                <label class="au-checkbox">
                                    <input type="checkbox" name="all" id="all"  class="all">
                                    <span class="au-checkmark"></span>
                                </label>
                            </th>
                            <th>Id</th>
                            <th>Email</th>
                            <th>Active</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($subscribers as $subscriber)
                            <tr class="">
                                <td>
                                    <label class="au-checkbox">
                                        <input type="checkbox" class="list_check" name="list[]" id="f" data-id="{{$subscriber->id}}">
                                        <span class="au-checkmark" ></span>
                                    </label>
                                </td>
                                <td class="pt-3-half" >{{$subscriber->id}}</td>
                                <td class="pt-3-half" >{{$subscriber->email}}</td>
                                <td class="pt-3-half" >
                                    <form action="{{route('admin.subscriberChange',$subscriber->id)}}" method="POST">
                                        @csrf
                                        @if($subscriber->active == true)
                                            <span class="table-active"><button type="submit" class="btn btn-success  btn-sm my-0">Active</button></span>
                                        @else
                                            <span class="table-active"><button type="submit" class="btn btn-danger  btn-sm my-0">Inactive</button></span>
                                        @endif
                                    </form>
                                </td>
                                <td>
                                    <div class="table-data-feature">
                                        <form action="{{ route('admin.subscriberDestroy',$subscriber->id)}}" method="POST">
                                            @csrf
                                            <button type="submit" class="item" data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-delete"></i></button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ $subscribers->links() }}
            </div>
        </div>
    </div>
@endsection
