@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mb-5 offset-xl-1">
            <div class="section-block">
                <h5 class="section-title">Edit Slider</h5>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="tab-regular">
                <ul class="nav nav-tabs " id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">English</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Armenia</a>
                    </li>
                </ul>
                <form method="post" action="{{ route('admin.sliders.update', $sliders->id) }}" enctype="multipart/form-data" >
                    @csrf
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <label for="title_en" class="col-form-label">Title En</label>
                            <input name="title_en" id="title_en" class="form-control" value = "{{old('title_en')? old('title_en'):$sliders->title_en}}">

                            <label for="subtitle_en" class="col-form-label">Subtitle En</label>
                            <input name="subtitle_en" id="subtitle_en" class="form-control" value = "{{old('subtitle_en')? old('subtitle_en'):$sliders->subtitle_en}}">
                        </div>

                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <label for="title_am" class="col-form-label">Title Am</label>
                            <input name="title_am" id="title_am" class="form-control" value = "{{old('title_am')? old('title_am'):$sliders->title_am}}">

                            <label for="subtitle_am" class="col-form-label">Subtitle Am</label>
                            <input name="subtitle_am" id="subtitle_am" class="form-control" value = "{{old('subtitle_am')? old('subtitle_am'):$sliders->subtitle_am}}">
                        </div>

                        <label for="image" class="col-form-label image">image <span class="asterisk">*</span></label>
                        <input type="file"  name="image" id="image" value="{{ $sliders->image}}"><br/><br/>
                        <img src="{{asset("storage/".$sliders->image . "_large" . "." . $sliders->ext)}}" width="300px" height="300px"><br/><br />

                        <button type="submit" name="create" class="btn btn-success btn-sm my-0"> Submit </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
