@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mb-5 offset-xl-1">
            <div class="section-block">
                <h5 class="section-title">Create News</h5>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="tab-regular">
                <ul class="nav nav-tabs " id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Armenia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">English</a>
                    </li>
                </ul>
                <form method="post" action="{{ route('admin.addNews') }}"  enctype="multipart/form-data" >
                    @csrf
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <label for="title_en" class="col-form-label">Title En </label>
                            <input type="text" name="title_en" id="title_en" class="form-control" value="{{old('title_en')}}">

                            <label for="description_en" class="col-form-label">Description En </label>
                            <textarea  name="description_en" id="description_en" class="form-control">{{old('description_en')}}</textarea>

                            <label for="text_en" class="col-form-label">Text En </label>
                            <textarea name="text_en" id="text_en" class="form-control">{{old('text_en')}}</textarea>

                        </div>
                        <div class="tab-pane fade  show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <label for="title_am" class="col-form-label">Title Am  <span class="asterisk">*</span></label>
                            <input type="text" name="title_am" id="title_am" class="form-control" value="{{old('title_am')}}">

                            <label for="description_am" class="col-form-label">Description Am <span class="asterisk">*</span></label>
                            <textarea  name="description_am" class="form-control" id="description_am" >{{old('description_am')}}</textarea>


                            <label for="text_am" class="col-form-label">Text Am <span class="asterisk">*</span></label>
                            <textarea name="text_am" id="text_am" class="form-control">{{old('description_am')}}</textarea>
                        </div>

{{--                        <label for="category" class="col-form-label">Category <span class="asterisk">*</span></label>--}}
{{--                        <select name="category_id" id="category" class="form-control">--}}
{{--                            <option value="0">no category</option>--}}
{{--                            @foreach ($categories as  $category)--}}
{{--                                <option value="{{ $category->id }}">{{ $category->title_en }}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}

                        <label for="cover" class="col-form-label image">Image <span class="asterisk">*</span></label>
                        <input type="file" name="cover" id="cover"><br/><br/>

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="defaultUnchecked" name="send_subscribers">
                            <label class="custom-control-label" for="defaultUnchecked">Send Subscribers</label>
                        </div><br/>


                        <button type="submit" name="create" class="btn btn-success  btn-sm my-0">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.config.autoParagraph = false;
    CKEDITOR.config.filebrowserBrowseUrl  = '/laravel-filemanager';
    CKEDITOR.config.filebrowserUploadUrl   = '/laravel-filemanager/upload';
    // CKEDITOR.replace( 'description_en');
    CKEDITOR.replace( 'text_en');
    // CKEDITOR.replace( 'description_am');
    CKEDITOR.replace( 'text_am');
</script>
@endsection
