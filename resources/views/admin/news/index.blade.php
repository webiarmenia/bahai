@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-5 m-b-35">News</h3>
                <div class="table-data__tool">
                    <div class="table-data__tool-left">
                        <div class="rs-select2--light rs-select2--sm">
                            @if (\Session::has('success'))
                                <div class="alert" style="background-color: #81dab5;width: 255px; color:#fff;font-size: 18px;text-align: center">
                                    {!! \Session::get('success') !!}
                                </div>
                            @endif
                            <a href="{{route('admin.news.create')}}"><button class="au-btn au-btn--green">Add News</button></a>
                        </div>
                        <div class="rs-select2--dark rs-select2--sm btn-delete">
                            <button class="au-btn au-btn-load delete" data-href="news/delete"  onclick="deleteData()" style="background-color: #a1a2a9;">Delete</button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive table--no-card m-b-30">
                    <table class="table table-borderless table-striped table-earning">
                        <thead class="">
                        <tr>
                            <th>
                                <label class="au-checkbox">
                                    <input type="checkbox" name="all" id="all"  class="all">
                                    <span class="au-checkmark"></span>
                                </label>
                            </th>
                            <th>Id</th>
                            <th>Title</th>
{{--                            <th>Text</th>--}}
{{--                            <th>Description</th>--}}
                            <th>Cover</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($news as $item)
                            <tr class="">
                                <td>
                                    <label class="au-checkbox">
                                        <input type="checkbox" class="list_check" name="list[]" id="f" data-id="{{$item->id}}">
                                        <span class="au-checkmark" ></span>
                                    </label>
                                </td>
                                <td class="pt-3-half" >{{$item->id}}</td>
                                <td class="pt-3-half" >{{$item->title_am}}</td>
{{--                                <td class="pt-3-half" >{{$item->text_en}}</td>--}}
{{--                                <td class="pt-3-half" >{{$item->description_am}}</td>--}}
                                <td><img src="{{asset("storage/".$item->cover. '_large' . '.' . $item->ext)}}" width="120" height="120" ></td>
                                <td>
                                    <div class="table-data-feature">
                                        <a class="item" data-toggle="tooltip" data-placement="top" href="{{ route('admin.news.edit',$item->id)}}"><i class="zmdi zmdi-edit"></i></a>
                                        <form action="{{route('admin.news.destroy',$item->id)}}" method="POST">
                                            @csrf
                                            <button type="submit" class="item" data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-delete"></i></button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ $news->links() }}
            </div>
        </div>
    </div>
@endsection
