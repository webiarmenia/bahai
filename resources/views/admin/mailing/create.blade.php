@extends('layouts.app')
@section('css')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" media="all">
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mb-5 offset-xl-1">
            <div class="section-block">
                <h5 class="section-title">Create Email</h5>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="tab-regular">
                <form method="POST" action="{{ route('admin.sendEmail') }}">
                    @csrf
                    <div class="tab-content" id="myTabContent">

                        <label for="subject" class="col-form-label">Subject <span class="asterisk">*</span></label>
                        <input type="text" name="title" id="subject" class="form-control" value="{{old('title')}}">

                        <label for="subscribers" class="col-form-label">Choose Subscribers <span class="asterisk">*</span></label><br/>

                        <select class="js-example-basic-multiple" name="subscribers[]" multiple="multiple" style="width: 100%!important;">
                            @foreach($subscribers as $subscriber)
                                <option value="{{$subscriber->email}}">{{$subscriber->email}}</option>
                            @endforeach
                        </select><br/>

                        <label for="message" class="col-form-label">Enter Message <span class="asterisk">*</span></label>
                        <textarea class="form-control" id="message" name="message">{{old('message')}}</textarea><br/>

                        <button type="submit" class="btn btn-danger" name="save">save Message's</button>
                        <button type="submit" class="btn btn-success" name="send">Send Message's</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="/js/select2.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.config.autoParagraph = false;
        CKEDITOR.config.filebrowserBrowseUrl  = '/laravel-filemanager';
        CKEDITOR.config.filebrowserUploadUrl   = '/laravel-filemanager/upload';
        CKEDITOR.replace( 'message');
    </script>
@endsection
