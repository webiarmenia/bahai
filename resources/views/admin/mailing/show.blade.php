@extends('layouts.app')
@section('content')
    <div class="col-xl-9 offset-xl-1 col-lg-12 col-md-12 col-sm-12 co-12">
        <section class="card card-fluid">
            <h5 class="card-header drag-handle"> Email </h5>
            <ul class="sortable-lists list-group list-group-flush list-group-bordered" id="items">
                <li class="list-group-item align-items-center drag-handle">
                    <span class="drag-indicator"></span>
                    <div class="row">
                        <div class="col-md-2">
                            <h2>Subject</h2>
                        </div>
                        <div class="col-md-10">
                            {{$email->title}}
                        </div>
                    </div>
                </li>
                <li class="list-group-item align-items-center drag-handle">
                    <span class="drag-indicator"></span>
                    <div class="row">
                        <div class="col-md-2">
                            <h2>Subscribers</h2>
                        </div>
                        <div class="col-md-10">
                            @foreach($emailSubscribers as $subscriber)
                                {{$subscriber}}<br>
                            @endforeach
                        </div>
                    </div>
                </li>
                <li class="list-group-item align-items-center drag-handle">
                    <span class="drag-indicator"></span>
                    <div class="row">
                        <div class="col-md-2">
                            <h2>Email</h2>
                        </div>
                        <div class="col-md-10">
                            {{$email->message}}
                        </div>
                    </div>
                </li>
            </ul>
        </section>
    </div>
@endsection