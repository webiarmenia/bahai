@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-5 m-b-35">Mailing</h3>
                <div class="table-data__tool">
                    <div class="table-data__tool-left">
                        <div class="rs-select2--light rs-select2--sm">
                            @if (\Session::has('success'))
                                <div class="alert" style="background-color: #81dab5;width: 255px; color:#fff;font-size: 18px;text-align: center">
                                    {!! \Session::get('success') !!}
                                </div>
                            @endif
                            <a href="{{route('admin.mailing.create')}}"><button class="au-btn au-btn--green ">Add Email</button></a>
                        </div>
                        <div class="rs-select2--dark rs-select2--sm btn-delete">
                            <button class="au-btn au-btn-load delete" data-href="mailing/delete" onclick="deleteData()" >Delete</button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive table--no-card m-b-30">
                    <table class="table table-borderless table-striped table-earning">
                        <thead class="">
                        <tr>
                            <th>
                                <label class="au-checkbox">
                                    <input type="checkbox" name="all" id="all"  class="all">
                                    <span class="au-checkmark"></span>
                                </label>
                            </th>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($emails as $email)
                            <tr class="">
                                <td>
                                    <label class="au-checkbox">
                                        <input type="checkbox" class="list_check" name="list[]" id="f" data-id="{{$email->id}}">
                                        <span class="au-checkmark" ></span>
                                    </label>
                                </td>
                                <td class="pt-3-half" >{{$email->id}}</td>
                                <td class="pt-3-half" >{{$email->title}}</td>
                                <td class="pt-3-half" >
                                    @if($email->status == true)
                                        <button type="submit" class="btn btn-success  btn-sm my-0">Sended</button>
                                    @else
                                        <button type="submit" class="btn btn-danger  btn-sm my-0">Seved</button>
                                    @endif
                                </td>
                                <td>
                                    <div class="table-data-feature">
                                    @if($email->status == true)
                                            <a class="item" data-toggle="tooltip" data-placement="top" href="{{ route('admin.mailing.show',$email->id)}}"><i class="zmdi zmdi-eye"></i></a>
                                    @else
                                            <a class="item" data-toggle="tooltip" data-placement="top" href="{{ route('admin.mailing.edit',$email->id)}}"><i class="zmdi zmdi-edit"></i></a>
                                        @endif
                                        <form action="{{route('admin.mailing.destroy',$email->id)}}" method="POST">
                                            @csrf
                                        <button type="submit" class="item" data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-delete"></i></button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ $emails->links() }}
            </div>
        </div>
    </div>
@endsection
