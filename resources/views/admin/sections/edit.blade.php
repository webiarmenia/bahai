@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mb-5 offset-xl-1">
            <div class="section-block">
                <h5 class="section-title">Edit Section</h5>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="tab-regular">
                <ul class="nav nav-tabs " id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">English</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Armenia</a>
                    </li>
                </ul>
                <form method="POST" action="{{ route('admin.sections.update', $item->id) }}" enctype="multipart/form-data" >
                    @csrf
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                            <label for="title_en" class="col-form-label">Title En <span class="asterisk">*</span></label>
                            <input type="text" name="title_en" id="title_en" class="form-control" value="{{old('title_en')? old('title_en'): $item->title_en}}">

                            <label for="description_en" class="col-form-label">Description En <span class="asterisk">*</span></label>
                            <textarea  name="description_en" id="description_en" class="form-control">{{old('description_en')? old('description_en'): $item->description_en}}</textarea>

                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <label for="title_am" class="col-form-label">Title Am <span class="asterisk">*</span></label>
                            <input type="text" name="title_am" id="title_am" class="form-control" value="{{old('title_am')? old('title_am'): $item->title_am}}">

                            <label for="description_am" class="col-form-label">Description Am <span class="asterisk">*</span></label>
                            <textarea  name="description_am" id="description_am" class="form-control">{{old('description_am')? old('description_am'): $item->description_am}}</textarea>
                        </div>

                        <label for="cover" class="col-form-label">Cover <span class="asterisk">*</span></label><br/>
                        <input type="file" name="cover" id="cover"><br/>
                        <input type="hidden" name="coverImage" value="{{$item->cover}}"><br/>
                        <img src="{{asset("storage/".$item->cover . "_large" . "." . $item->ext)}}" width="300px" height="300px"><br/><br />

                        <button type="submit" name="create" class="btn btn-success  btn-sm my-0">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.config.autoParagraph = false;
        CKEDITOR.config.filebrowserBrowseUrl  = '/laravel-filemanager';
        CKEDITOR.config.filebrowserUploadUrl   = '/laravel-filemanager/upload';
        CKEDITOR.replace( 'description_en');
        CKEDITOR.replace( 'description_am');
    </script>
@endsection

