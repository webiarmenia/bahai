<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>bahai</title>
    <link rel="icon"  href="/storage/image/favicon.ico">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href='/fontawesome/css/all.css' type='text/css' rel='stylesheet'>
    <link href="/css/homepage.css " rel="stylesheet">
</head>
<body>
<div class="home-page">
    <div class="container">
        <section class="header-page">
            <div><nav class="navbar navbar-expand-lg navbar-light nav nav-bar">
                   <div class="nav-bar-homepage"><a class="navbar-brand nav-home-page" href="#">Bahai.org Home</a></div>
                    <button class="navbar-toggler third-button  responsive-button" type="button" data-toggle="collapse"
                            id="menu-opener" data-target="#navbarSupportedContent22"
                            aria-controls="navbarSupportedContent22" aria-expanded="false" aria-label="Toggle navigation">
                        <div class="animated-icon3"><span></span><span></span><span></span></div>
                        <span class="header-block-menu">Menu</span>
                    </button>
                    <div class="collapse navbar-collapse  navbar-custom" id="navbarSupportedContent22">
                        <ul class="navbar-nav mr-auto custom-navbar-nav">
                            <li class="nav-item custom-nav-item" id="menu-nav-item-1">
                                <span class="menu"> What Bahá’ís Believe</span>
                                <span class="caret"> <i class="fa fa-caret-down caret-down"></i></span>
                            </li>
                            <li class="nav-item custom-nav-item" id="menu-nav-item-2">
                                <span class="menu">What Bahá’ís Do </span>
                                <span class="caret"> <i class="fa fa-caret-down caret-down"></i></span>
                            </li>
                            <li class="nav-item custom-nav-item" id="menu-nav-item-3">
                                <span class="menu">Bahá’í Reference Library</span>
                                <span class="caret"> <i class="fa fa-caret-down caret-down"></i></span>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="header-search">
                    <form class="form-inline my-2 my-lg-0 form-inline-flex" action="">
                        <div class="languages">
                            <div class="lang-en"><span> Armenian</span>
                                <i class="fa fa-caret-down caret-icon-down"></i>
                            </div>
                        </div>
                        <div class="lang">
                            <i class="fa fa-caret-up caret-icon-up"></i>
                            <div class="lang-ar">English</div>
                        </div>
                        <input class="form-control mr-sm-2 search " type="text" placeholder="" >
                        <button type="" class="btn-search "> <i class="fa fa-search"></i> </button>
                    </form>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hidden-search" >
                <div class="menu-search-block">
                    <div class="animated-icon2"><span></span><span></span><span></span></div>
                </div>
                <div class="btn-search-hidden"> <i class="fa fa-search"></i></div>
            </div>
                <form class="form-inline my-2 my-lg-0 menu-search-hidden" action="" >
                    <input class="form-control mr-sm-2 search " type="text" placeholder="" >
                    <div class="btn-search "> <i class="fa fa-search"></i> </div>
                    <ul class="">
                        <li ><div class="search-hidden-block"><span class="open-li"></span><a href="#">Search Bahai.org </a></div></li>
                        <li ><div class="search-hidden-block"><span class="open-li"></span><a href="#">Search the Bahá’í Reference Library</a></div></li>
                    </ul>
                </form>
            <div class="submenu menu-nav-item-1-content">
                <div class="row hidden-menu-open" id="menu">
                    <div class="col-lg-4  col-12 hidden-menu">
                        <ul class="list-items">
                            <li id="menu-item-1"> Lorem ipsum dolor sit amet</li>
                            <li id="menu-item-2"> Lorem ipsum dolor sit</li>
                            <li id="menu-item-3"> Lorem ipsum dolor</li>
                            <li id="menu-item-4"> Lorem ipsum</li>
                            <li id="menu-item-5"> Lorem</li>
                        </ul>
                    </div>
                    <div class="col-lg-8  hidden-menu-info">
                        <p class="menu-item-1-content hidden-menu-info-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p class="menu-item-2-content hidden-menu-info-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        </p>
                        <p class="menu-item-3-content hidden-menu-info-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse

                        </p>
                        <p class="menu-item-4-content hidden-menu-info-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        </p>
                        <p class="menu-item-5-content hidden-menu-info-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        </p>
                    </div>
                </div>
            </div>
            <div class="submenu menu-nav-item-2-content">
                <div class="row hidden-menu-open">
                    <div class="col-lg-4  col-12 hidden-menu">
                        <ul class="list-items">
                            <li id="menu-item-1"> 1Lorem ipsum dolor sit amet</li>
                            <li id="menu-item-2"> 1Lorem ipsum dolor sit</li>
                            <li id="menu-item-3"> 1Lorem ipsum dolor</li>
                            <li id="menu-item-4"> 1Lorem ipsum</li>
                            <li id="menu-item-5"> 1Lorem</li>
                        </ul>
                    </div>
                    <div class="col-lg-8 hidden-menu-info">
                        <p class="hidden-menu-info-text menu-item-1-content">
                            2 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p class="hidden-menu-info-text menu-item-2-content">
                            2Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        </p>
                        <p class="hidden-menu-info-text menu-item-3-content">
                            2Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse

                        </p>
                        <p class="hidden-menu-info-text menu-item-4-content">
                            2Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        </p>
                        <p class="hidden-menu-info-text menu-item-5-content">
                            2Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        </p>
                    </div>
                </div>
            </div>
            <div class="submenu menu-nav-item-3-content">
                <div class="row hidden-menu-open">
                    <div class="col-lg-4  col-12 hidden-menu">
                        <ul class="list-items">
                            <li id="menu-item-1"> 2Lorem ipsum dolor sit amet</li>
                            <li id="menu-item-2"> 2Lorem ipsum dolor sit</li>
                            <li id="menu-item-3"> 2Lorem ipsum dolor</li>
                            <li id="menu-item-4"> 2Lorem ipsum</li>
                            <li id="menu-item-5"> 2Lorem</li>
                        </ul>
                    </div>
                    <div class="col-lg-8 hidden-menu-info">
                        <p class="hidden-menu-info-text menu-item-1-content">
                            3Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p class="hidden-menu-info-text menu-item-2-content">
                            3Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        </p>
                        <p class="hidden-menu-info-text menu-item-3-content">
                            3Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse

                        </p>
                        <p class="hidden-menu-info-text menu-item-4-content">
                            3Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        </p>
                        <p class="hidden-menu-info-text menu-item-5-content">
                            3Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        </p>
                    </div>
                </div>
            </div>
            <div class="bahai-faith">
                <p class="title">The Bahá’í Faith</p>
                <p id="text">The website of the worldwide Bahá’í community</p>
            </div>
            <div class="header-homepage-image">
                <img class="header-page-image" src="/storage/image/aaa.jpg" alt="">
            </div>
        </section>

        <section class="news">
            <div class="row news-pages">
                <div class="col-md-4 news-page">
                    <div class="description">
                        <a href=""><img src="/storage/image/f1a3e4d3-606.png" alt=""></a>
                        <div class="news-text">
                            <p class="news-title">Lorem ipsum</p>
                            <div class="news-description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing
                                elit, sed do eiusmodtempor incididunt ut labore et
                                dolore magna aliqua. Ut enim ad minim veniam,quis
                                nostrud exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit essecillum dolore
                                eu fugiat nulla pariatur.Duis aute irure dolor in.
                            </div>
                            <button class="news-btn" type="button"> Read more</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 news-page">
                    <div class="description">
                        <a href=""><img src="/storage/image/f1a3e4d3-606.png" alt=""></a>
                        <div class="news-text">
                            <p class="news-title">Lorem ipsum</p>
                            <div class="news-description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing
                                elit, sed do eiusmodtempor incididunt ut labore et
                                dolore magna aliqua. Ut enim ad minim veniam,quis
                                nostrud exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit essecillum dolore
                                eu fugiat nulla pariatur.Duis aute irure dolor in.
                            </div>
                            <button class="news-btn" type="button"> Read more</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 news-page">
                    <div class="description">
                        <a href=""><img src="/storage/image/f1a3e4d3-606.png" alt=""></a>
                        <div class="news-text">
                            <p class="news-title">Lorem ipsum</p>
                            <div class="news-description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing
                                elit, sed do eiusmodtempor incididunt ut labore et
                                dolore magna aliqua. Ut enim ad minim veniam,quis
                                nostrud exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit essecillum dolore
                                eu fugiat nulla pariatur.Duis aute irure dolor in.
                            </div>
                            <button class="news-btn" type="button"> Read more</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="baha-u-llah">
            <div class="row">
                <div class="col-md-8 offset-md-1 col-10 offset-1">
                    <p class="baha-u-llah-title">“Let your vision be world embracing…” — <span>Bahá’u’lláh</span></p>
                    <div>
                        <p class="baha-u-llah-text1">
                            Throughout history, God has sent to humanity a series of divine Educators—known as
                            Manifestations of God—whose teachings have provided the basis for the advancement of
                            civilization.
                            These Manifestations have included Abraham, Krishna, Zoroaster, Moses, Buddha, Jesus, and
                            Muhammad. Bahá’u’lláh, the latest of these Messengers, explained that the religions of the
                            world come from the same Source and are in essence successive chapters of one religion from
                            God.
                        </p>
                        <p class="baha-u-llah-text2">
                            Bahá’ís believe the crucial need facing humanity is to find a unifying vision of the future
                            of society and of the nature and purpose of life. Such a vision unfolds in the writings of
                            Bahá’u’lláh.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 offset-md-0 col-10 offset-1 baha-llah">
                    <img class="baha-u-llah-image" src="/storage/image/s.png" alt="">
                <div class="llah">
                    <p class="llah-title">BICENTENARY OF THE BIRTH OF</p>
                    <p class="llah-text">VISIT SITE</p>
                </div>
                </div>
            </div>
        </section>

        <section class="baha_is_believe">
            <div class="row">
                <div class="col-md-8 offset-md-1 col-10 offset-1">
                    <a href=''><p class="baha_is_believe_text">What Bahá’ís Believe »</p>
                        <img class="baha_is_believe-img" src="/storage/image/4be46ef2.png" alt="">
                    </a>
                    <a href="">
                        <div class="row baha_is_believe-bottom">
                            <div class="col-md-5 col-5">
                                <div class="image-highlight-list">
                                    <img src="/storage/image/0307c691.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-7 offset-md-0 col-12 offset-0">
                                <h3 class="link-title">Bahá’u’lláh and His Covenant</h3>
                                <p class="link-subtitle">The origins of the Bahá’í Faith and the source of its distinctive unity        </p>
                            </div>
                        </div>
                    </a>
                    <a href="">
                        <div class="row baha_is_believe-bottom">
                            <div class="col-md-5 col-5">
                                <div class="image-highlight-list">
                                    <img src="/storage/image/0307c691.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-7 offset-md-0 col-12 offset-0">
                                <h3 class="link-title">Bahá’u’lláh and His Covenant</h3>
                                <p class="link-subtitle">The origins of the Bahá’í Faith and the source of its distinctive unity        </p>
                            </div>
                        </div>
                    </a>
                    <a href="">
                        <div class="row baha_is_believe-bottom">
                            <div class="col-md-5 col-5">
                                <div class="image-highlight-list">
                                    <img src="/storage/image/0307c691.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-7 offset-md-0 col-12 offset-0">
                                <h3 class="link-title">Bahá’u’lláh and His Covenant</h3>
                                <p class="link-subtitle">The origins of the Bahá’í Faith and the source of its distinctive unity        </p>
                            </div>
                        </div>
                    </a>
                    <a href="">
                        <div class="row baha_is_believe-bottom">
                            <div class="col-md-5 col-5">
                                <div class="image-highlight-list">
                                    <img src="/storage/image/0307c691.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-7 offset-md-0 col-12 offset-0">
                                <h3 class="link-title">Bahá’u’lláh and His Covenant</h3>
                                <p class="link-subtitle">The origins of the Bahá’í Faith and the source of its distinctive unity        </p>
                            </div>
                        </div>
                    </a>
                    <a href="">
                        <div class="row baha_is_believe-bottom">
                            <div class="col-md-5 col-5">
                                <div class="image-highlight-list">
                                    <img src="/storage/image/0307c691.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-7 offset-md-0 col-12 offset-0">
                                <h3 class="link-title">Bahá’u’lláh and His Covenant</h3>
                                <p class="link-subtitle">The origins of the Bahá’í Faith and the source of its distinctive unity        </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 offset-md-0"></div>
            </div>
        </section>

        <section class="baha-is-do">
            <div class="row">
                <div class="col-md-8 offset-md-1 col-sm-10 offset-sm-1 col-10 offset-1">
                    <a href=""><p class="baha-is-do-title">What Bahá’ís Do »</p></a>
                    <img class="baha-is-do-img" src="http://bahai.loc/storage/image/ae600154.png" alt="">
                    <div class="row baha-is-do-bottom">
                        <div class="col-md-6">
                            <a href=''><p class="baha-is-do-text1">Response to the Call of Bahá’u’lláh </p></a>
                            <p class="baha-is-do-text2">The integration of service and worship in Bahá’í community
                                life</p>
                        </div>
                        <div class="col-md-6">
                            <img class="baha-is-do-image" src="http://bahai.loc/storage/image/b21ff8bd.png" alt="">
                        </div>
                    </div>
                    <div class="row baha-is-do-bottom">
                        <div class="col-md-6">
                            <a href=''><p class="baha-is-do-text1">Response to the Call of Bahá’u’lláh </p></a>
                            <p class="baha-is-do-text2">The integration of service and worship in Bahá’í community
                                life</p>
                        </div>
                        <div class="col-md-6">
                            <img class="baha-is-do-image" src="http://bahai.loc/storage/image/b21ff8bd.png" alt="">
                        </div>
                    </div>
                    <div class="row baha-is-do-bottom">
                        <div class="col-md-6">
                            <a href=''><p class="baha-is-do-text1">Response to the Call of Bahá’u’lláh </p></a>
                            <p class="baha-is-do-text2">The integration of service and worship in Bahá’í community
                                life</p>
                        </div>
                        <div class="col-md-6">
                            <img class="baha-is-do-image" src="http://bahai.loc/storage/image/b21ff8bd.png" alt="">
                        </div>
                    </div>
                    <div class="row baha-is-do-bottom">
                        <div class="col-md-6">
                            <a href=''><p class="baha-is-do-text1">Response to the Call of Bahá’u’lláh </p></a>
                            <p class="baha-is-do-text2">The integration of service and worship in Bahá’í community
                                life</p>
                        </div>
                        <div class="col-md-6">
                            <img class="baha-is-do-image" src="http://bahai.loc/storage/image/b21ff8bd.png" alt="">
                        </div>
                    </div>
                    <div class="row baha-is-do-bottom">
                        <div class="col-md-6">
                            <a href=''><p class="baha-is-do-text1">Response to the Call of Bahá’u’lláh </p></a>
                            <p class="baha-is-do-text2">The integration of service and worship in Bahá’í community
                                life</p>
                        </div>
                        <div class="col-md-6">
                            <img class="baha-is-do-image" src="http://bahai.loc/storage/image/b21ff8bd.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-sm-10 offset-sm-1 col-md-3 offset-md-0 col-10 offset-1">
                    <p class="baha-is-do-text">
                        “If the learned and worldly-wise men of this age were to allow mankind to inhale the
                        fragrance of fellowship and love, every understanding heart would apprehend the meaning
                        of true liberty, and discover the secret of undisturbed peace and absolute composure.”
                    </p>
                    <p class="baha-is-do-text3">– Bahá’u’lláh</p>
                </div>
            </div>
        </section>

        <section class="bahai-library">
            <div class="row">
                <div class="col-md-9 offset-md-1 col-sm-9 offset-sm-1 col-9 offset-1">
                    <p class="bahai-library-title">Bahá’í Reference Library »</p>
                    <img class='bahai-library-image' src="/storage/image/91fcb55.png" alt="">
                    <p class="bahai-library-text">
                        The Bahá’í Reference Library is the authoritative online source of Bahá’í writings.
                        It contains selected works of Bahá’u’lláh, the Báb, ‘Abdu’l-Bahá, Shoghi Effendi,
                        and the Universal House of Justice, as well as other Bahá’í texts.
                    </p>
                    <div class="contact-us ">
                        <img src="/storage/image/77.png" alt="">
                        <div><button class="button-contact-us" type="text">contact-us</button></div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </section>

        <section class="footer">
            <div class="row footer-contents">
                <img src="/storage/image/a39016.png" alt="">
                <div class="col-md-3 col-sm-4 col-lg-3 information col-6 offset-0">
                    <a href=""><p class='footer-title'>A Global Community</p>
                    <p class="footer-text">
                        The Bahá’í Faith is established in more than 100,000 localities in virtually every
                        country and territory around the world.
                    </p>
                    <p class="footer-text">National Bahá’í websites »</p></a>
                </div>
                <div class="col-md-4 col-sm-5 offset-sm-3 offset-md-5 col-lg-3 offset-lg-6 col-6 offset-0 other-sites">
                    <a href=""><p class='other-sites-link'>The Life of Bahá’u’lláh </p></a>
                    <a href=""><p class='other-sites-link'>The Universal House of Justice</p></a>
                    <a href=""><p class='other-sites-link'>Bicentenary of the Birth of Bahá’u’lláh</p></a>
                    <a href=""><p class='other-sites-link'>A Widening Embrace</p></a>
                    <a href=""><p class='other-sites-link'>Light to the World</p></a>
                    <a href=""><p class='other-sites-link'>Frontiers of Learning</p></a>
                    <a href=""><p class='other-sites-link'>Bahá’í World News Service</p></a>
                </div>
            </div>

        </section>
        <section class="footer-content">
                <div class="row">
                    <div class="col-md-4 footer-menu col-sm-12 ">
                        <ul>
                            <li ><a href="">Contact</a></li>
                            <li ><a href="">Legal</a></li>
                            <li ><a href="">Privacy</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-12  footer-logo">
                        <a href="">
                            <img class="logo" src="/storage/image/478886548.png" alt="logo">
                            <p class="baha-faith">The Bahá’í Faith</p>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-12 bahai">
                        <p class="bahai-int-com">© 2019 Bahá’í International Community</p>
                    </div>
                </div>
        </section>
    </div>
</div>
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/homepage.js" type="text/javascript"></script>
</body>
</html>

