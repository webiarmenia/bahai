<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'bahai',
            'email' => 'bahai@gmail.com',
            'password' => bcrypt('eigh4iap5jah1quaiFoo'),
        ]);
    }
}
