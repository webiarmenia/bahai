<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescToLinks extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('links', function (Blueprint $table) {
            $table->string('text_en')->nullable()->change();
            $table->string('text_am')->nullable()->change();
            $table->string('link')->nullable()->change();
            $table->string('page_id');
            $table->string('image')->nullable()->change();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('links', function (Blueprint $table) {
            $table->text('text_en')->change();
            $table->text('text_am')->change();
            $table->string('image')->change();
            $table->string('link')->change();
            $table->dropColumn('page_id');
        });
    }
}