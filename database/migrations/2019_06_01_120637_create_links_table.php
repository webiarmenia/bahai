<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('text_en');
            $table->text('text_am');
            $table->string('image');
            $table->string('link');
            $table->string('ext')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('links');
    }
}
