<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menus extends Model
{
    protected $fillable = ['title_en','title_arm','parent_id','type','description_en','description_am','url','order','slug'];
    protected $table = 'menus';

    public function parent(){
        return $this->hasOne(Menus::class,'id','parent_id');
    }

    public function child() {
        return $this->hasMany('App\Menus','parent_id','id') ;
    }



    public function page(){
        return $this->hasOne(Page::class, 'id', 'page_id');
    }

}
