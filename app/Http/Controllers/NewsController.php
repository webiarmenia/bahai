<?php

namespace App\Http\Controllers;

use App\Email;
use App\Mail\SendEmail;
use App\News;
//use App\Category;
use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;
use File;
use Mail;

class NewsController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $news = News::orderBy('id', 'DESC')->paginate(10);
        return view('admin.news.index')->with([
            'news' => $news
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function addNews(Request $request){
        $subscribers = Subscriber::where('active', true)->get();
        $subscribersArr = [];
        foreach ($subscribers as $subscriber) {
            array_push($subscribersArr, $subscriber->email);
        }
        $this->validate($request, [
            'cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title_am' => 'required',
//            'title_en' => 'required',
//            'description_en' => 'required',
            'description_am' => 'required',
//            'text_en' => 'required',
            'text_am' => 'required',
//            'category_id' => 'required',
        ]);
        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'news/';
        $file = Input::file('cover');
        if ($request->hasfile('cover')){
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $cover = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }
            }
        }else{
            $cover = null;
            $ext =  null;
        }
        if ($request->send_subscribers != null) {
            $createEmail = Email::insert([
                'title' => $request->title_en,
                'subscribers' => json_encode($subscribersArr),
                'message' => $request->description_en,
                'status' => true,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
            $subject = $request->title_en;
            $message = $request->description_en;
            $link = env('APP_URL');
            Mail::to($subscribersArr)->send(new SendEmail($subject, $message, $link));
            $send_subscribers = true;
        } else {
            $send_subscribers = false;
        }
        $createNews = News::insert([
//            'category_id' => $request->category_id,
            'title_en' => $request->title_en,
            'title_am' => $request->title_am,
            'description_en' => $request->description_en,
            'description_am' => $request->description_am,
            'text_en' => $request->text_en,
            'text_am' => $request->text_am,
            'cover'   => $cover,
            'ext'     => $ext,
            'send_subscribers' => $send_subscribers,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        if ($createNews && isset($createEmail)) {
            return redirect()->route('admin.news')->with('success', 'News created and email is sended');
        } else if ($createNews) {
            return redirect()->route('admin.news')->with('success', 'News created');
        } else {
            return back();
        }
    }

    public function updateNews(Request $request, $id){
        $editedNews = News::where('id', $id)->firstOrfail();
        $this->validate($request, [
            'cover' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title_am' => 'required',
//            'title_en' => 'required',
//            'description_en' => 'required',
            'description_am' => 'required',
//            'text_en' => 'required',
            'text_am' => 'required',
//            'category_id' => 'required',
        ]);

        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'news/';
        $file = Input::file('cover');
        if ($request->hasfile('cover')) {
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $namePath = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }

                $file_name_old = $editedNews->cover . '_' . $type . '.' . $editedNews->ext;
                if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                    unlink(storage_path().'/app/public'  . '/'. $file_name_old);
                }
            }
        }
        else {
            $namePath = $editedNews->cover;
            $ext =$editedNews->ext;
        }
        if ($editedNews->send_subsribers != true && $request->send_subscribers != null) {
            $subscribers = Subscriber::where('active', true)->get();
            $subscribersArr = [];
            foreach ($subscribers as $subscriber) {
                array_push($subscribersArr, $subscriber->email);
            }
            $createEmail = Email::insert([
                'title' => $request->title_en,
                'subscribers' => json_encode($subscribersArr),
                'message' => $request->description_en,
                'status' => true,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
            $subject = $request->title_en;
            $message = $request->description_en;
            $link = env('APP_URL');
            Mail::to($subscribersArr)->send(new SendEmail($subject, $message, $link));
        }
        if ($request->send_subscribers != null && $editedNews->send_subscribers == false) {
            $send_subscribers = true;
        } else {
            $send_subscribers = $editedNews->send_subscribers;
        }
        if ($editedNews) {
            $updateNews = News::where('id', $id)->update([
//                'category_id' => $request->category_id,
                'title_en' => $request->title_en,
                'title_am' => $request->title_am,
                'description_en' => $request->description_en,
                'description_am' => $request->description_am,
                'text_en' => $request->text_en,
                'text_am' => $request->text_am,
                'cover'   => $namePath,
                'ext'     => $ext,
                'send_subscribers' => $send_subscribers,
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        }

        if ($updateNews && isset($createEmail)) {
            return redirect()->route('admin.news')->with('success', 'News was updated and email sended');
        } else if ($updateNews) {
            return redirect()->route('admin.news')->with('success', 'News was updated');
        } else {
            return back();
        }
    }

    public function create(){
//        $categories = Category::all();
        return view('admin.news.create');
//        return view('admin.news.create')->with([
//            'categories' => $categories
//        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $item = News::where('id', $id)->firstOrfail();
        return view('admin.news.show')->with([
            'item' => $item
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
//        $categories = Category::all();
        $item = News::where('id', $id)->firstOrfail();
        return view('admin.news.edit')->with([
            'item' => $item,
//            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){
        $news = News::where('id',$id)->firstOrfail();
        $sizes_name = array('large', 'medium', 'small');
        foreach ($sizes_name as $key => $type) {
            $file_name_old = $news->cover . '_' . $type . '.' . $news->ext;
            if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                unlink(storage_path().'/app/public'  . '/'. $file_name_old);
            }
        }
        $news->delete();
        return redirect()->route('admin.news')->with('success', 'News successfully deleted');
    }

    public function deleteAll(Request $request){
        $news = News::whereIn('id', $request->get('arr'))->get();
        $sizes_name = array('large', 'medium', 'small');
        if ($news) {
            foreach ($news as $item) {
                foreach ($sizes_name as $key => $type) {
                    $file_name_old = $item->cover . '_' . $type . '.' . $item->ext;
                    if (is_file(storage_path() . '/app/public/' .$file_name_old)) {
                        unlink(storage_path() . '/app/public/' . $file_name_old);
                    }
                }
            }
            $news = News::whereIn('id', $request->get('arr'))->delete();
        } else {
            abort(404);
        }
    }
}
