<?php

namespace App\Http\Controllers;

use App\Section;
use Illuminate\Http\Request;
use Image;
use File;
use Illuminate\Support\Facades\Input;

class SectionsController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $sections = Section::orderBy('id','DESC')->paginate(10);
        return view('admin.sections.index')->with([
            'sections' => $sections
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin.sections.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $this->validate($request, [
            'cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title_am' =>'required',
            'title_en' =>'required',
            'description_en' =>'required',
            'description_am' =>'required',
        ]);

        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'section/';
        $file = Input::file('cover');
        if ($request->hasfile('cover')){
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $cover = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }
            }
        }else{
            $cover = null;
            $ext =  null;
        }
        $createSection = Section::insert([
            'title_en' => $request->title_en,
            'title_am' => $request->title_am,
            'description_en' => $request->description_en,
            'description_am' => $request->description_am,
            'cover' => $cover,
            'ext'   => $ext,
            'created_at' =>  \Carbon\Carbon::now()->toDateTimeString()
        ]);

        if($createSection){
            return redirect()->route('admin.sections')->with('success', 'Section created');
        }else{
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $item = Section::where('id',$id)->firstOrfail();
        return view('admin.sections.show')->with([
            'item' => $item,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $item = Section::where('id',$id)->firstOrfail();
        return view('admin.sections.edit')->with([
            'item' => $item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $editedSection = Section::where('id',$id)->firstOrfail();
        $this->validate($request, [
            'cover' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title_am' =>'required',
            'title_en' =>'required',
            'description_en' =>'required',
            'description_am' =>'required',
        ]);
        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'section/';
        $file = Input::file('cover');
        if ($request->hasfile('cover')) {
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $namePath = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }
                $file_name_old = $editedSection->cover . '_' . $type . '.' . $editedSection->ext;
                if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                    unlink(storage_path().'/app/public'  . '/'. $file_name_old);
                }
            }
        }
        else {
            $namePath = $editedSection->cover;
            $ext =$editedSection->ext;
        }
        if ($editedSection){
            $updateSection = Section::where('id', $id)->update([
                'title_en' => $request->title_en,
                'title_am' => $request->title_am,
                'description_en' => $request->description_en,
                'description_am' => $request->description_am,
                'cover' => $namePath,
                'ext'   => $ext,
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        }
        if($updateSection){
            return redirect()->route('admin.sections')->with('success', 'Section was updated');
        }else{
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){
        $section = Section::where('id',$id)->firstOrfail();
        $sizes_name = array('large', 'medium', 'small');
        foreach ($sizes_name as $key => $type) {
            $file_name_old = $section->cover . '_' . $type . '.' . $section->ext;
            if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                unlink(storage_path().'/app/public'  . '/'. $file_name_old);
            }
        }
        if ($section) {
            $section->delete();
        }
        return redirect()->route('admin.sections')->with('success', 'Sections successfully deleted');
    }
    public function deleteAll(Request $request){
        $section = Section::whereIn('id', $request->get('arr'))->get();
        $sizes_name = array('large', 'medium', 'small');
        if ($section) {
            foreach ($section as $item) {
                foreach ($sizes_name as $key => $type) {
                    $file_name_old = $item->cover . '_' . $type . '.' . $item->ext;
                    if (is_file(storage_path() . '/app/public/' .$file_name_old)) {
                        unlink(storage_path() . '/app/public/' . $file_name_old);
                    }
                }
            }
            $section = Section::whereIn('id', $request->get('arr'))->delete();
        } else {
            abort(404);
        }
    }
}
