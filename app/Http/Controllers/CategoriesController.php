<?php

namespace App\Http\Controllers;

use App\Category;
use App\MenuCategories;
use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;


class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::orderBy('id','DESC')->paginate(10);
        return view('admin.categories.index')->with([
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('id','DESC')->get();
        return view('admin.categories.create')->with([
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'cover' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title_am' =>'required',
//            'title_en' =>'required',
        ]);
        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'category/';
        $file = Input::file('cover');
        if ($request->hasfile('cover')){
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $cover = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }
            }
        }else{
            $cover = null;
            $ext =  null;
        }
        $createCategory = Category::insert([
            'parent_id' => $request->parent_id,
            'title_en' => $request->title_en,
            'title_am' => $request->title_am,
            'cover' => $cover,
            'ext'   => $ext,
            'created_at' =>  \Carbon\Carbon::now()->toDateTimeString()
        ]);

        if($createCategory){
            return redirect()->route('admin.categories')->with('success', 'Category was created');
        }else{
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $categories = Category::where('id','!=',$id)->get();
        $item = Category::where('id',$id)->firstOrfail();
        return view('admin.categories.edit')->with([
            'item' => $item,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editedCategory = Category::where('id',$id)->firstOrfail();
        $this->validate($request, [
            'cover' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title_am' =>'required',
//            'title_en' =>'required',
        ]);
        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'category/';
        $file = Input::file('cover');
        if ($request->hasfile('cover')) {
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $namePath = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }
                $file_name_old = $editedCategory->cover . '_' . $type . '.' . $editedCategory->ext;
                if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                    unlink(storage_path().'/app/public'  . '/'. $file_name_old);
                }
            }
        }
        else {
            $namePath = $editedCategory->cover;
            $ext =$editedCategory->ext;
        }
        if ($editedCategory){
            $updateCategory = Category::where('id', $id)->update([
                'parent_id' => $request->parent_id,
                'title_en' => $request->title_en,
                'title_am' => $request->title_am,
                'cover' => $namePath,
                'ext'   => $ext,
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        }
        if($updateCategory){
            return redirect()->route('admin.categories')->with('success', 'Category was updated');
        }else{
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){
        $category = Category::where('id',$id)->firstOrfail();
        $sizes_name = array('large', 'medium', 'small');
        foreach ($sizes_name as $key => $type) {
            $file_name_old = $category->cover . '_' . $type . '.' . $category->ext;
            if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                unlink(storage_path().'/app/public'  . '/'. $file_name_old);
            }
        }
        $category->delete();
        return redirect()->route('admin.categories')->with('success', 'Category successfully deleted');
    }
    public function deleteAll(Request $request){
        $categories = Category::whereIn('id', $request->get('arr'))->get();
        $sizes_name = array('large', 'medium', 'small');
        if ($categories) {
            foreach ($categories as $item) {
                foreach ($sizes_name as $key => $type) {
                    $file_name_old = $item->cover . '_' . $type . '.' . $item->ext;
                    if (is_file(storage_path() . '/app/public/' .$file_name_old)) {
                        unlink(storage_path() . '/app/public/' . $file_name_old);
                    }
                }
            }
            $categories = Category::whereIn('id', $request->get('arr'))->delete();
        } else {
            abort(404);
        }
    }
}
