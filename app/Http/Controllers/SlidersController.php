<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use Illuminate\Support\Facades\Input;
use File;
use Image;

class SlidersController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $sliders = Slider::orderBy('id', 'desc')->paginate(10);
        return view('admin.slider.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'slider/';
        $file = Input::file('image');
        if ($request->hasfile('image')){
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $cover = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('image')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('image')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('image')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }
            }
        }else{
            $cover = null;
            $ext =  null;
        }
        $sliders = new Slider();
        $sliders->title_en      = $request->title_en;
        $sliders->title_am      = $request->title_am;
        $sliders->subtitle_en   = $request->subtitle_en;
        $sliders->subtitle_am   = $request->subtitle_am;
        $sliders->image         = $cover;
        $sliders->ext           = $ext;
        $sliders->save();
        return redirect()->route('admin.sliders')->with('success', 'Sliders created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $sliders = Slider::where('id',$id)->firstOrfail();
        return view('admin.slider.edit')->with([
            'sliders' => $sliders,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $editedSlider = Slider::where('id',$id)->firstOrfail();
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'slider/';
        $file = Input::file('image');
        if ($request->hasfile('image')) {
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $namePath = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('image')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('image')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('image')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }
                $file_name_old = $editedSlider->image . '_' . $type . '.' . $editedSlider->ext;
                if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                    unlink(storage_path().'/app/public'  . '/'. $file_name_old);
                }
            }
        }
        else {
            $namePath = $editedSlider->cover;
            $ext =$editedSlider->ext;
        }
        $sliders = Slider::where('id',$id)->firstOrfail();
        $sliders->title_en = $request->get('title_en');
        $sliders->title_am = $request->get('title_am');
        $sliders->subtitle_en = $request->get('subtitle_en');
        $sliders->subtitle_am = $request->get('subtitle_am');
        $sliders->image       = $namePath;
        $sliders->ext         = $ext;
        $sliders->save();
        return redirect()->route('admin.sliders')->with('success', 'Sliders was updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){
        $sliders = Slider::where('id',$id)->firstOrfail();
        $sizes_name = array('large', 'medium', 'small');
        foreach ($sizes_name as $key => $type) {
            $file_name_old = $sliders->image . '_' . $type . '.' . $sliders->ext;
            if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                unlink(storage_path().'/app/public'  . '/'. $file_name_old);
            }
        }
        $sliders->delete();
        return redirect()->route('admin.sliders')->with('Sliders', 'Pages successfully deleted');
    }
    public function deleteAll(Request $request){
        $sliders = Slider::whereIn('id', $request->get('arr'))->get();
        $sizes_name = array('large', 'medium', 'small');
        if ($sliders) {
            foreach ($sliders as $item) {
                foreach ($sizes_name as $key => $type) {
                    $file_name_old = $item->image . '_' . $type . '.' . $item->ext;
                    if (is_file(storage_path() . '/app/public/' .$file_name_old)) {
                        unlink(storage_path() . '/app/public/' . $file_name_old);
                    }
                }
            }
            $sliders = Slider::whereIn('id', $request->get('arr'))->delete();
        } else {
            abort(404);
        }
    }
}
