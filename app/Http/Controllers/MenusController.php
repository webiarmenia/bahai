<?php

namespace App\Http\Controllers;

use App\Category;
use App\MenuCategories;
use App\Page;
use Illuminate\Http\Request;
use App\Menus;
use Illuminate\Support\Facades\Input;
use Image;


class MenusController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $menus = Menus::orderBy('id','DESC')->paginate(10);
        return view('admin.menus.index')->with([
            'menus' => $menus,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request){
        $menus = Menus::where('parent_id',0)->get();
        $categories = Category::all();
        $pages = Page::all();
        return view('admin.menus.create')->with([
            'menus' => $menus,
            'categories' => $categories,
            'pages' => $pages,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
        $this->validate($request, [
            'cover' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title_am'      => 'required',
            'parent_id'      => 'required',
            'type'           => 'required',
            'description_am' => 'required',
            'url'            => 'required_without_all:slug',
            'url'            => 'nullable | active_url',
            'slug'            => 'required_without_all:url',
            'slug'            => 'nullable|alpha_dash | max:25 | unique:menus',


            'order'          => 'required',
//            'categories'     => 'required',
        ]);
        if ($request->active == 'on'){
            $active = true;
        }else{
            $active = false;
        }
        if ($request->homepage_show == 'on'){
            $homepage_show = true;
        }else{
            $homepage_show = false;
        }
        if ($request->url_enable == 'on'){
            $url_enable = true;
        }else{
            $url_enable = false;
        }
        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'menus/';
        $file = Input::file('cover');
        if ($request->hasfile('cover')){
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $cover = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }
            }
        }else{
            $cover = null;
            $ext =  null;
        }
        $menus = new Menus();
//        if($request->slug){
//            $request->url =  null;
//        }else if( $request->url){
//            $request->slug = null;
//        }
        $menus->title_en       = $request->title_en;
        $menus->title_am       = $request->title_am;
        $menus->parent_id      = $request->parent_id;
        $menus->type           = $request->type;
        $menus->description_en = $request->description_en;
        $menus->description_am = $request->description_am;
        $menus->slug           = $request->slug;
        $menus->url            = $request->url;
        $menus->order          = $request->order;
        $menus->page_id        = $request->page_id;
        $menus->active         = $active;
        $menus->homepage_show  = $homepage_show;
        $menus->url_enable     = $url_enable;
        $menus->cover  = $cover;
        $menus->ext  = $ext;

        $saved = $menus->save();
//        $categories_arr = [];
//        if ($request->categories != null){
//            foreach ($request->categories as $category){
//                array_push($categories_arr,array('menu_id' => $menus->id,'category_id' => $category));
//            }
//
//            MenuCategories::insert($categories_arr);
//        }
        if ($saved){
            return redirect()->route('admin.menus')->with('success', 'Menu successfully created');
        }else{
            return back();
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menus = Menus::where('parent_id',0)->where('id','!=',$id)->get();
        $menu = Menus::find($id);
        $categories = Category::all();
        $menu_categories = MenuCategories::where('menu_id',$id)->select('category_id')->get();
        $menu_categories_array = [];
        $pages = Page::all();
        foreach ($menu_categories as $item){
            array_push($menu_categories_array,$item->category_id);
        }
        return view('admin.menus.edit')->with([
            'menus' => $menus,
            'menu' => $menu,
            'categories' => $categories,
            'menu_categories_array' => $menu_categories_array,
            'pages' => $pages
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $editedMenu = Menus::where('id',$id)->firstOrfail();
        $this->validate($request, [
            'cover' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title_am'      => 'required',
            'parent_id'      => 'required',
            'type'           => 'required',
            'description_am' => 'required',
            'url'            => 'required_without_all:slug',
            'url'            => 'nullable | active_url',
            'slug'            => 'required_without_all:url',
            'slug'            => 'nullable|alpha_dash | max:25 | unique:menus,slug,'.$id,
//            'url'            => 'required_without_all:slug',
////            'slug'            => 'required_without_all:url',
//            'slug'            => 'required_without_all:url | alpha_dash | max:25 | unique:menus,slug,'.$id,
////            'url'            => 'required | alpha_dash | max:25 | unique:menus,url,'.$id,
            'order'          => 'required',
        ]);

        if ($request->active == 'on'){
            $active = true;
        }else{
            $active = false;
        }
        if ($request->homepage_show == 'on'){
            $homepage_show = true;
        }else{
            $homepage_show = false;
        }
        if ($request->url_enable == 'on'){
            $url_enable = true;
        }else{
            $url_enable = false;
        }
        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'menus/';
        $file = Input::file('cover');
        if ($request->hasfile('cover')) {
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $namePath = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }

                $file_name_old = $editedMenu->cover . '_' . $type . '.' . $editedMenu->ext;
                if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                    unlink(storage_path().'/app/public'  . '/'. $file_name_old);
                }
            }
        }
        else {
            $namePath = $editedMenu->cover;
            $ext =$editedMenu->ext;
        }
        $menu = Menus::where('id',$id)->firstOrfail();

        $menu->title_en         = $request->get('title_en');
        $menu->title_am         = $request->get('title_am');
        $menu->parent_id        = $request->get('parent_id');
        $menu->type             = $request->get('type');
        $menu->description_en   = $request->get('description_en');
        $menu->description_am   = $request->get('description_am');
        $menu->url              = $request->get('url');
        $menu->slug             = $request->get('slug');
        $menu->order            = $request->get('order');
        $menu->page_id          = $request->get('page_id');
        $menu->url_enable       = $url_enable;
        $menu->active           = $active;
        $menu->homepage_show    = $homepage_show;
        $menu->cover            = $namePath;
        $menu->ext              = $ext;
        $saved = $menu->save();
//        $categories_arr = [];
//        if ($request->categories != null){
//            $menuCategories = MenuCategories::where('menu_id',$id)->get()->toArray();
//            if (!empty($menuCategories)){
//                MenuCategories::where('menu_id',$id)->delete();
//            }
//            foreach ($request->categories as $category){
//                array_push($categories_arr,array('menu_id' => $id,'category_id' => $category));
//            }
//
//            MenuCategories::insert($categories_arr);
//        }
        if ($saved){
            return redirect()->route('admin.menus')->with('success', 'Menus was updated');
        }else{
            return back();
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){
        $menu = Menus::where('id',$id)->firstOrfail();
        $sizes_name = array('large', 'medium', 'small');
        foreach ($sizes_name as $key => $type) {
            $file_name_old = $menu->cover . '_' . $type . '.' . $menu->ext;
            if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                unlink(storage_path().'/app/public'  . '/'. $file_name_old);
            }
        }
        $menu->delete();
        return redirect()->route('admin.menus')->with('success', 'Menus successfully deleted');
    }

    public function deleteAll(Request $request){
        $menu = Menus::whereIn('id', $request->get('arr'))->get();
        $sizes_name = array('large', 'medium', 'small');
        if ($menu) {
            foreach ($menu as $item) {
                foreach ($sizes_name as $key => $type) {
                    $file_name_old = $item->cover . '_' . $type . '.' . $item->ext;
                    if (is_file(storage_path() . '/app/public/' .$file_name_old)) {
                        unlink(storage_path() . '/app/public/' . $file_name_old);
                    }
                }
            }
            $menu = Menus::whereIn('id', $request->get('arr'))->delete();
        } else {
            abort(404);
        }
    }
}
