<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;


class PageController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $pages = Page::orderBy('id', 'desc')->paginate(10);
        return view('admin.page.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $request->validate([
            'title_am'=>'required',
//            'title_en'=>'required',
            'description_am'=> 'required',
//            'description_en'=> 'required',
            'text_am' =>'required',
//            'text_en' =>'required',
            'cover' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'pages/';
        $file = Input::file('cover');
        if ($request->hasfile('cover')){
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $cover = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }
            }
        }else{
            $cover = null;
            $ext =  null;
        }
        $pages = new Page();
        $pages->title_am        = $request->get('title_am');
        $pages->title_en        = $request->get('title_en');
        $pages->description_am  = $request->get('description_am');
        $pages->description_en  = $request->get('description_en');
        $pages->text_en         = $request->get('text_en');
        $pages->text_am         = $request->get('text_am');
        $pages->cover           = $cover;
        $pages->ext             = $ext;

        $saved = $pages->save();
        if ($saved){
            return redirect()->route('admin.pages')->with('success', 'Pages created');
        }else{
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $pages = Page::where('id',$id)->firstOrfail();
        return view('admin.page.edit')->with([
            'pages' => $pages,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id){
        $editedPage = Page::where('id',$id)->firstOrfail();
        $request->validate([
            'title_am'=>'required',
//            'title_en'=>'required',
            'description_am'=> 'required',
//            'description_en'=> 'required',
            'text_am'=> 'required',
//            'text_en'=> 'required',
            'cover' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'pages/';
        $file = Input::file('cover');
        if ($request->hasfile('cover')) {
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $namePath = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('cover')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }

                $file_name_old = $editedPage->cover . '_' . $type . '.' . $editedPage->ext;
                if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                    unlink(storage_path().'/app/public'  . '/'. $file_name_old);
                }
            }
        }
        else {
            $namePath = $editedPage->cover;
            $ext =$editedPage->ext;
        }
        $pages = Page::where('id',$id)->firstOrfail();
        $pages->title_am = $request->get('title_am');
        $pages->title_en = $request->get('title_en');
        $pages->description_am = $request->get('description_am');
        $pages->description_en = $request->get('description_en');
        $pages->text_am = $request->get('text_am');
        $pages->text_en = $request->get('text_en');
        $pages->cover = $namePath;
        $pages->ext   = $ext;
        $pages->save();
        return redirect()->route('admin.pages')->with('success', 'Pages was updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $pages = Page::where('id',$id)->firstOrfail();
        $sizes_name = array('large', 'medium', 'small');
        foreach ($sizes_name as $key => $type) {
            $file_name_old = $pages->cover . '_' . $type . '.' . $pages->ext;
            if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                unlink(storage_path().'/app/public'  . '/'. $file_name_old);
            }
        }
        $pages->delete();
        return redirect()->route('admin.pages')->with('success', 'Pages successfully deleted');
    }
    public function deleteAll(Request $request){
        $pages = Page::whereIn('id', $request->get('arr'))->get();
        $sizes_name = array('large', 'medium', 'small');
        if ($pages) {
            foreach ($pages as $item) {
                foreach ($sizes_name as $key => $type) {
                    $file_name_old = $item->cover . '_' . $type . '.' . $item->ext;
                    if (is_file(storage_path() . '/app/public/' .$file_name_old)) {
                        unlink(storage_path() . '/app/public/' . $file_name_old);
                    }
                }
            }
            $pages = Page::whereIn('id', $request->get('arr'))->delete();
        } else {
            abort(404);
        }
    }
}
