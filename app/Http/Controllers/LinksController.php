<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;
use App\Page;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Support\Facades\Validator;
use Image;

class LinksController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $links = Link::orderBy('id', 'desc')->paginate(10);
        return view('admin.link.index')->with([
            'links' => $links,
            ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
        $page_id =Page::all();
        return view('admin.link.create')->with([
            'page_id' => $page_id,
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
        $this->validate($request,[
            'link' => 'required_without_all:text_en,text_am,image',
            'text_en' => 'required_without_all:link,image',
            'text_am' => 'required_without_all:link,image',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|required_without_all:text_en,text_am,link',
        ]);

        if ($request->text_am != null || $request->text_en != null){
            $this->validate($request,[
                'text_en' => 'required_without:text_en',
                'text_am' => 'required_without:text_am',
            ]);
        }
        if ($request->order != null){
            $order = $request->order;
        }else{
            $order = 1;
        }
        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'link/';
        $file = Input::file('image');
        if ($request->hasfile('image')){
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $cover = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('image')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('image')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('image')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }
            }
        }else{
            $cover = null;
            $ext =  null;
        }
        $links = new Link([
            'text_en'=> $request->get('text_en'),
            'text_am'=> $request->get('text_am'),
            'link'=> $request->get('link'),
            'page_id'=> $request->get('page_id'),
            'image'  => $cover,
            'ext'    => $ext,
            'order'  => $order
        ]);
        $links->save();
        return redirect()->route('admin.links')->with('success', 'Links created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id){

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id){
        $links = Link::where('id',$id)->firstOrfail();
        $page_id =Page::all();
        return view('admin.link.edit')->with([
            'links' => $links,
            'page_id'=>$page_id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id){
        $editedLink = Link::where('id',$id)->firstOrfail();
        $this->validate($request,[
            'link' => 'required_without_all:text_en,text_am,image',
            'text_en' => 'required_without_all:link,image',
            'text_am' => 'required_without_all:link,image',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|required_without_all:text_en,text_am,link',
        ]);

        if ($request->text_am != null || $request->text_en != null){
            $this->validate($request,[
                'text_en' => 'required_without:text_en',
                'text_am' => 'required_without:text_am',
            ]);
        }
        if ($request->order != null){
            $order = $request->order;
        }else{
            $order = 1;
        }
        $sizes = array(array('248', '115'), array('300', '156'));
        $sizes_name = array('small','medium','large');
        $targetPath = 'link/';
        $file = Input::file('image');
        if ($request->hasfile('image')) {
            $fname = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameWithOutExt = str_replace('.' . $ext, '', $fname);
            $nameWith = str_random(32);
            $namePath = $targetPath. $nameWith;
            foreach ($sizes_name as $key => $type) {
                $nameWithExt = $nameWith . '_' . $type . '.' . $ext;
                if ($type == "medium") {
                    $file_hash = $request->file('image')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[1][0], $sizes[1][1], null, "top-left")->save();
                } else if ($type == "small") {
                    $file_hash = $request->file('image')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->fit($sizes[0][0], $sizes[0][1], null, "top-left")->save();
                }else if ($type == "large") {
                    $file_hash = $request->file('image')->storeAs('public/' . $targetPath, $nameWithExt);
                    $source = storage_path() . '/app/public/' . $targetPath . $nameWithExt;
                    $image = Image::make($source)->save();
                }
                $file_name_old = $editedLink->image . '_' . $type . '.' . $editedLink->ext;
                if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                    unlink(storage_path().'/app/public'  . '/'. $file_name_old);
                }
            }
        }
        else {
            $namePath = $editedLink->image;
            $ext =$editedLink->ext;
        }
        $links = Link::where('id',$id)->firstOrfail();
        $links->text_am = $request->get('text_am');
        $links->text_en = $request->get('text_en');
        $links->link = $request->get('link');
        $links->page_id = $request->get('page_id');
        $links->order = $order;
        $links->image =$namePath;
        $links->ext =$ext;
        $links->save();
        return redirect()->route('admin.links')->with('success', 'Links was updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){
        $links = Link::where('id',$id)->firstOrfail();
        $sizes_name = array('large', 'medium', 'small');
        foreach ($sizes_name as $key => $type) {
            $file_name_old = $links->image . '_' . $type . '.' . $links->ext;
            if (is_file(storage_path().'/app/public'  . '/'. $file_name_old)) {
                unlink(storage_path().'/app/public'  . '/'. $file_name_old);
            }
        }
        $links->delete();
        return redirect()->route('admin.links')->with('success', 'Links successfully deleted');
    }


    public function deleteAll(Request $request){
        $links = Link::whereIn('id', $request->get('arr'))->get();
        $sizes_name = array('large', 'medium', 'small');
        if ($links) {
            foreach ($links as $item) {
                foreach ($sizes_name as $key => $type) {
                    $file_name_old = $item->image . '_' . $type . '.' . $item->ext;
                    if (is_file(storage_path() . '/app/public/' .$file_name_old)) {
                        unlink(storage_path() . '/app/public/' . $file_name_old);
                    }
                }
            }
            $links = Link::whereIn('id', $request->get('arr'))->delete();
        } else {
            abort(404);
        }
    }
}
