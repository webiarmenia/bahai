<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $settings = Setting::orderBy('id','DESC')->paginate(10);
        return view('admin.settings.index')->with([
            'settings' => $settings
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $this->validate($request, [
            'key' => 'required',
            'value_am' =>'required',
            'value_en' =>'required',
        ]);

        $creatSetting = Setting::insert([
            'key' => $request->key,
            'value_en' => $request->value_en,
            'value_am' => $request->value_am,
            'created_at' =>  \Carbon\Carbon::now()->toDateTimeString()
        ]);

        if($creatSetting){
            return redirect()->route('admin.settings')->with('success', 'Setting created');
        }else{
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $item = Setting::where('id',$id)->firstOrfail();
        return view('admin.settings.show')->with([
            'item' => $item,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $item = Setting::where('id',$id)->firstOrfail();
        return view('admin.settings.edit')->with([
            'item' =>$item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $editedSetting = Setting::where('id',$id)->firstOrfail();
        $this->validate($request, [
            'value_am' =>'required',
            'value_en' =>'required',
        ]);
        if($editedSetting){
            $updateSetting = Setting::where('id', $id)->update([
                'value_en' => $request->value_en,
                'value_am' => $request->value_am,
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        }
        if($updateSetting){
            return redirect()->route('admin.settings')->with('success', 'Setting was updated');
        }else{
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $setting = Setting::where('id', $id)->firstOrfail();
        if ($setting) {
            $setting->delete();
        }
        return redirect()->route('admin.settings')->with('success', 'Settings successfully deleted');
    }

    public function deleteAll(Request $request){
        $setting = Setting::whereIn('id',$request->get('arr'))->delete();
    }
}
