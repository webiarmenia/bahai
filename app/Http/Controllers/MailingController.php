<?php

namespace App\Http\Controllers;

use App\Email;
use App\Subscriber;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use Auth;
use Mail;
use Form;

class MailingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $emails = Email::orderBy('id','DESC')->paginate(10);
        return view('admin.mailing.index')->with([
            'emails' => $emails,
        ]);
    }

    public function sendEmail(Request $request){
        $subject = $request->title;
        $message = $request->message;
        $request->validate([
            'title' => 'required',
            'subscribers' => 'required',
            'message' => 'required',
        ]);
        if($request->has('save')){
           $subscribers = json_encode($request->subscribers);
           $createEmail = Email::insert([
               'title' => $request->title,
               'subscribers' => $subscribers,
               'message' => $request->message,
               'status' => false,
               'created_at' => \Carbon\Carbon::now()->toDateTimeString()
           ]);
           if($createEmail){
               return redirect()->route('admin.mailing')->with('success', 'Emails is saving');
           }
        }
        if($request->has('send')){
            $subscribers = json_encode($request->subscribers);
            $createEmail = Email::insert([
                'title' => $request->title,
                'subscribers' => $subscribers,
                'message' => $request->message,
                'status' => true,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
            Mail::to($request->subscribers)->send(new SendEmail($subject,$message))->from();
            if($createEmail){
                return redirect()->route('admin.mailing')->with('success', 'Emails is send and saved');
            }else{
                return back();
            }
        }
    }

    public function updateEmail(Request $request,$id){
        $subject = $request->title;
        $message = $request->message;
        $request->validate([
            'title' => 'required',
            'subscribers' => 'required',
            'message' => 'required',
        ]);
        if($request->has('save')){
            $subscribers = json_encode($request->subscribers);
            $updateEmail = Email::where('id',$id)->update([
                'title' => $request->title,
                'subscribers' => $subscribers,
                'message' => $request->message,
                'status' => false,
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        }

        if($request->has('send')){
            $subscribers = json_encode($request->subscribers);
            $updateEmail = Email::where('id',$id)->update([
                'title' => $request->title,
                'subscribers' => $subscribers,
                'message' => $request->message,
                'status' => true,
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
            Mail::to($request->subscribers)->send(new SendEmail($subject,$message));
            if($updateEmail){
                return redirect()->route('admin.mailing')->with('success', 'Emails is send and saved');
            }
        }
        if($updateEmail){
            return back()->with('success', 'Emails is updated');
        }else{
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $subscribers = Subscriber::where('active',true)->get();
        return view('admin.mailing.create')->with([
            'subscribers' => $subscribers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $email = Email::where('id',$id)->where('status',true)->firstOrfail();
        $emailSubscribers = json_decode($email->subscribers);
        return view('admin.mailing.show')->with([
            'email' => $email,
            'emailSubscribers' => $emailSubscribers,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $subscribers = Subscriber::where('active',true)->get();
        $email = Email::where('id',$id)->where('status',false)->firstOrfail();
        $emailSubscribers = json_decode($email->subscribers);
        return view('admin.mailing.edit')->with([
            'subscribers' => $subscribers,
            'email' => $email,
            'emailSubscribers' => $emailSubscribers
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $email = Email::where('id',$id)->firstOrfail();
        if($email){
            $email->delete();
        }
        return redirect()->route('admin.mailing')->with('success', 'Mailing successfully deleted');
    }
    public function deleteAll(Request $request){
        $email = Email::whereIn('id',$request->get('arr'))->delete();
    }
}
