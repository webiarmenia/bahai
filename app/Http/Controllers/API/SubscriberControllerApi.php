<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscriber;
use Validator;
use Illuminate\Support\Carbon;
class SubscriberControllerApi extends Controller
{

    /**
     * Create
     */
    public function postCreate(Request $request)
    {

        $rules = [
            'email'    => 'required|unique:subscribers,email',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {

            return response()->json([
                'message' => $validator->messages(),
            ]);
        } else {
            $postArray = [
                'email'      => $request->email,
                'active'     => false,
                'created_at' => Carbon::now(),

            ];

            $subscriber = Subscriber::insert($postArray);

            if($subscriber) {

                return response()->json([
                    'email'        => $request->email,
                    'active'       => false,
                ]);
            } else {
                return response()->json([
                    'message' => 'Registration failed, please try again.',
                ]);
            }
        }
    }

}
