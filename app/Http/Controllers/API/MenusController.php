<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Link;
use App\Page;
use App\Mail\SendEmail;
use App\MenuCategories;
use App\News;
use App\Post;
use App\Setting;
use App\Section;
use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menus;
use App;
use Mail;



class MenusController extends Controller
{

    public function __construct(Request $request)
    {
        $language = $request->header('language');
        if (isset($language)) {
            if ($language == 'en' || $language == 'am') {
                $this->lang = $language;
            } else {
                $this->lang = 'en';
            }

        } else {
            $this->lang = 'en';
        }
    }

    public function getMenus(Request $request)
    {
        if ($request->parent_id) {
            $parentId = $request->parent_id;
        } else {
            $parentId = 0;
        }

        if ($request->menus_type) {
            $menus_type = $request->menus_type;
            $data = Menus::where('type', $menus_type)->orderBy('order', 'DESC')->get();
        } else {
            $data = Menus::orderBy('order', 'desc')->get();
        }


        return $this->buildTree($data, $parentId);
    }

    protected function buildTree($elements, $parentId)
    {

        $menus = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;

                }
                $menus[] = $element;
            }
        }

        return $menus;

    }

    public function getHeaderMenu($api = false)
    {
        $lang = $this->lang;
        $menus = Menus::where('type', 1)
            ->where('active', true)
            ->where('homepage_show', true)
            ->where('parent_id', 0)
            ->orderBy('order', 'DESC')
            ->where('title_'.$lang, "!=", null)->orWhere('description_'.$lang, "!=", null)
            ->with('child')
            ->get()
            ->toArray();
        $menus_array = [];
        //get Menu where type is Header Menu
        if (!empty($menus)) {
            foreach ($menus as $menu) {
                $child = [];
                if (!empty($menu['child'])) {
                    foreach ($menu['child'] as $item) {
                        if( $item['title_' . $lang] !=null && $item['description_' . $lang] != null){
                            $child[] = [
                                'id' => $item['id'],
                                'title' => $item['title_' . $lang],
                                'description' => $item['description_' . $lang],
                                'order' => $item['order'],
                                'slug'  => $item['url'],
                                'url_enable' =>$item['url_enable'],
                                'cover' => $item['cover'],
                                'ext'   => $item['ext'],
                            ];
                        }
                    }
                }
                if($menu['url_enable'] ==1){
                    $slug =  $menu['url'];
                }else if($menu['url_enable'] ==0){
                    $slug =  $menu['slug'];
                }
                array_push($menus_array, [
                    'id' => $menu['id'],
                    'title' => $menu['title_' . $lang],
                    'description' => $menu['description_' . $lang],
                    'slug' => $slug,
                    'url_enable' =>$menu['url_enable'],
                    'order' => $menu['order'],
                    'child' => $child
                ]);
            }

            if ($api == true) {
                return $menus_array;
            } else {
                return response()->json($menus_array);
            }
        } else {
            if ($api == true) {
                return $menus_array;
            } else {
                return response()->json($menus_array);
            }
        }

    }


    public function getfooterBottomMenu($api = false)
    {
        $lang = $this->lang;
//        $menus = Menus::where('type', 3)->with('page')->get()->toArray();
        $menus = Menus::where('type', 3)->where('title_'.$lang, "!=", null)->orWhere('description_'.$lang, "!=", null)->with('page')->get()->toArray();
        $menus_array = [];
        foreach ($menus as $menu) {
            if($menu['url_enable'] ==1){
                $slug =  $menu['url'];
            }else if($menu['url_enable'] ==0){
                $slug =  $menu['slug'];
            }
            array_push($menus_array, [
                'id' => $menu['id'],
                'title' => $menu['title_' . $lang],
                'description' => $menu['description_' . $lang],
                'order' => $menu['order'],
                'slug' => $slug,
                'url_enable' =>$menu['url_enable'],
                'page' => [
                    'id' => $menu['page']['id'],
                    'title' => $menu['page']['title_' . $lang],
                    'description' => $menu['page']['description_' . $lang],
                    'text' => $menu['page']['text_' . $lang],
                    'cover' => $menu['page']['cover'],
                    'ext' => $menu['page']['ext'],
                ]
            ]);

        }
        if ($api == false) {
            return response()->json($menus_array);
        } else {
            return $menus_array;
        }
    }

    public function getFooterMenu($api = false)
    {
        $lang = $this->lang;
//        $menus = Menus::where('type', 2)->with('page')->get()->toArray();
        $menus = Menus::where('type', 2)->where('title_'.$lang, "!=", null)->orWhere('description_'.$lang, "!=", null)
//            ->with('page')
            ->get()->toArray();
        $menus_array = [];
        foreach ($menus as $menu) {
            if($menu['url_enable'] ==1){
                $slug =  $menu['url'];
            }else if($menu['url_enable'] ==0){
                $slug =  $menu['slug'];
            }
            array_push($menus_array, [
                'id' => $menu['id'],
                'title' => $menu['title_' . $lang],
                'description' => $menu['description_' . $lang],
                'order' => $menu['order'],
                'slug' => $slug,
                'url_enable' =>$menu['url_enable']
//                'page' => [
//                    'id' => $menu['page']['id'],
//                    'title' => $menu['page']['title_' . $lang],
//                    'description' => $menu['page']['description_' . $lang],
//                    'text' => $menu['page']['text_' . $lang],
//                    'cover' => $menu['page']['cover'],
//                    'ext' => $menu['page']['ext'],
//                ]
            ]);

        }
        if ($api == false) {
            return response()->json($menus_array);
        } else {
            return $menus_array;
        }
    }

    public function getLastNews()
    {
        $lang = $this->lang;
        $news = News::orderBy('id', 'DESC')->where('title_'.$lang, "!=", null)->orWhere('description_'.$lang, "!=", null)->orWhere('text_'.$lang, "!=", null)->take(3)->get()->toArray();
        $news_array = [];
        foreach ($news as $news) {
            $news_array[] = [
                'id' => $news['id'],
                'title' => $news['title_' . $lang],
                'description' => $news['description_' . $lang],
                'text' => $news['text_' . $lang],
                'cover' => $news['cover'],
                'ext' => $news['ext'],
            ];
        }
        return $news_array;
    }

    public function getLastPosts(){
        $posts = Post::orderBy('id','DESC')->select('id','category_id','title_en','title_am','description_en','description_am','cover','ext')->take(5)->get()->toArray();
        $posts_array = [];
        foreach ($posts as $post){
            $posts_array[] = $post;
        }
        return $posts_array;
    }

    public function getRandomSlider(){
        $lang = $this->lang;
        $slider = Slider::inRandomOrder()->first();
        $slider_array = [];
        if ($slider){
            $slider_array = [
                'id' =>$slider['id'],
                'title' =>$slider['title_'.$lang],
                'subtitle' =>$slider['subtitle_'.$lang],
                'image' =>$slider['image'],
                'ext'   =>$slider['ext'],
            ];
            return $slider_array;
        }else{
            return null;
        }
    }

    public function getAllLinks(){
        $lang = $this->lang;
//        $links = Link::orderBy('order','ASC')->select('id','link','text_am','text_en','image','ext','page_id')->get()->toArray();
        $links = Link::orderBy('order','ASC')->Where('text'.$lang, "!=", null)->get()->toArray();
        $links_array = [];
        if (!empty($links)) {
            foreach ($links as $item) {
                $links_array[] = [
                    'id' =>$item['id'],
                    'link' =>$item['link'],
                    'text' =>$item['text_'.$lang],
                    'image' =>$item['image'],
                    'ext'   =>$item['ext'],
                    'page_id' =>$item['page_id'],
                ];
            }
        }
        return response()->json($links_array);
    }

    public function getLink(Request $request,$api = false){
        $lang = $this->lang;
        $this->validate($request, [
            'id' => 'required'
        ]);
        $links = Link::where('page_id', $request->id)->orderBy('order', 'ASC')->select('link', 'text_am', 'text_en', 'image','ext')->get()->toArray();
        $links_array = [];
        if (!empty($links)) {
            foreach ($links as $item) {
                $links_array[] = [
//                    'id' =>$item['id'],
                    'link' =>$item['link'],
                    'text' =>$item['text_'.$lang],
                    'image' =>$item['image'],
                    'ext'   =>$item['ext'],
//                    'page_id' =>$item['page_id'],
                ];
            }
        }
        if ($api == false){
            return response()->json($links_array);
        }else{
            return $links_array;
        }
    }

    public function getPageId($slug){
        $lang = $this->lang;
        if (!isset($slug) || $slug == null){
            return response()->json(['error' => 'Bad request']);
        }
        $menus = Menus::where('url',$slug)->first();
        $page_arr =[];
        $request =  new \Illuminate\Http\Request();
        if($menus){
            $page = Page::where("id", $menus->page_id)->first();
            if($page) {
                $request->replace(['id' => $page->id]);
                    $page_arr = [
                        'title' => $page['title_' . $lang],
                        'description' => $page['description_' . $lang],
                        'text'  => $page['text_' . $lang],
                        'cover' => $page['cover'],
                        'ext'   => $page['ext'],
                        'links' => $this->getLink($request, true)
                    ];
                return response()->json($page_arr);
            }else{
                return response()->json($page_arr);
            }
        } else{
            return response()->json($page_arr);
        }
    }

    public function getSection(Request $request){
        $lang = $this->lang;
        $this->validate($request, [
            'id' => 'required'
        ]);

        $category = Category::where('id',$request->id)->select('id','parent_id','title_en','title_am','cover','ext')->with('news')->first();
        $array = [];
        if ($category){
            foreach ($category->news as $news ){
                array_push($array,[
                    'id' => $news['id'],
                    'title' => $news['title_'.$lang],
                    'description' => $news['description_'.$lang],
                    'text'  => $news['text_'.$lang],
                    'cover' => $news['cover'],
                    'ext'   => $news['ext'],
//                    'send_subscribers' => $news->send_subscribers,
                    'category_id' => $news['category_id'],
                ]);
            }
            return response()->json([
                'id' => $category->id,
                'title' => $category['title_'.$lang],
                'cover' => $category->cover,
                'ext'   => $category->ext,
                'news'  => $array,
            ]);
        }else{
            return response()->json($array);
        }

    }

    public function homeDataSections(){
        $lang = $this->lang;
        $menus = Menus::where('type',1)
            ->where('active',true)
            ->where('homepage_show',true)
            ->where('parent_id',0)
            ->where('title_'.$lang, "!=", null)->orWhere('description_'.$lang, "!=", null)
            ->orderBy('order','DESC')
            ->with('child')
            ->get()
            ->toArray();
        $menus_array = [];
        //get Menu where type is Header Menu
        if (!empty($menus)) {
            foreach ($menus as $menu) {

                $child= [];
                if (!empty($menu['child'])){
                    foreach ($menu['child'] as $item){
//                        $page = App\Page::where('id',$item['page_id'])->first();dd($page);

                        $page = App\Page::where('id',$item['page_id'])->
                        where('title_'.$lang, "!=", null)->orWhere('description_'.$lang, "!=", null)->orWhere('text_'.$lang, "!=", null)
                            ->first();
                        if ($page) {
                            $child[] = [
                                'id' => $item['id'],
                                'slug' =>$item['url'],
                                'title' => $item['title_' . $lang],
                                'description' => $page['description_' . $lang],
                                'cover' => $page['cover'],
                                'ext'   => $page['ext']
                            ];
                        }
                    }
                }
                if($menu['url_enable'] ==1){
                    $slug =  $menu['url'];
                }else if($menu['url_enable'] ==0){
                    $slug =  $menu['slug'];
                }
                array_push($menus_array,[
                    'id' => $menu['id'],
                    'title' => $menu['title_'.$lang],
                    'description' => $menu['description_'.$lang],
                    'slug'  => $slug,
                    'url_enable' =>$menu['url_enable'],
                    'order' => $menu['order'],
                    'cover' => $menu['cover'],
                    'ext'   => $menu['ext'],
                    'child' => $child
                ]);
            }
                return $menus_array;
        }else{
                return $menus_array;
        }
    }

    public function getBanners($api = false){
        $lang = $this->lang;
        $sections = Section::orderBy('id','DESC')->select('id','title_en','title_am','description_en','description_am','cover','ext')->take(2)->get()->toArray();
        $sections_array = [];
        if (!empty($sections)){
            foreach ($sections as $section){
                $sections_array[] = [
                    'id' => $section['id'],
                    'title' => $section['title_'.$lang],
                    'description' => $section['description_'.$lang],
                    'cover' => $section['cover'],
                    'ext'   => $section['ext'],
                ];
            }
        }
        if ($api == true){
            return $sections_array;
        }else{
            return response()->json($sections_array);
        }
    }

    public function getData(){
        $request = new \Illuminate\Http\Request();
        $request->replace(['id' => 'home']);
        return response()->json([
            'header_menu' => $this->getHeaderMenu(true),
            'right_sidebar' =>$this->getLink($request,true),
            'settings' => $this->getSetting(),
            'footer_menu' => $this->getfooterMenu(true),
            'footer_bottom_menu' => $this->getfooterBottomMenu(true),
        ]);
    }
    public function getHomeContent(){
//        $request = new \Illuminate\Http\Request();
//        $request->replace(['id' => 'home']);
        return response()->json([
            'slider' => $this->getRandomSlider(),
            'banners' => $this->getBanners(true),
            'sections' => $this->homeDataSections(),
            'news' => $this->getLastNews(),
        ]);
    }
    public function getHomeData(){
        $request = new \Illuminate\Http\Request();

        $request->replace(['id' => 'home']);
        return response()->json([
            'header_menu' => $this->getHeaderMenu(true),
            'slider' => $this->getRandomSlider(),
            'right_sidebar' =>$this->getLink($request,true),
            'banners' => $this->getBanners(true),
            'sections' => $this->homeDataSections(),
            'settings' => $this->getSetting(),
            'news' => $this->getLastNews(),
//            'posts' => $this->getLastPosts(),
            'footer_menu' => $this->getfooterMenu(true),
            'footer_bottom_menu' => $this->getfooterBottomMenu(true),
        ]);
    }

    public function contactUs(Request $request){
        $this->validate($request,[
            'email' => 'required | email',
            'subject' => 'required',
            'message' => 'required ',
        ]);

        $app_email = env('APP_EMAIL');
        $request->replace(['to' => $app_email]);

        Mail::send(new App\Mail\ContactMail($request));
        return response()->json(['success' => true]);
    }

    public function getSetting(){
        $lang = $this->lang;
        $settings = Setting::where('key','copyright')->get()->toArray();
        $settings_array = [];
        if (!empty($settings)) {
            foreach ($settings as $item) {
                $settings_array[] = [
                    'key' =>$item['key'],
                    'value' =>$item['value_'. $lang],
                ];
            }
        }
        return  $settings_array;
    }
}

