<?php

namespace App\Http\Controllers\API;

use App\News;
use App\Page;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller{

    public function __construct(Request $request){
        $language = $request->header('language');
        if(isset($language)) {
            if ($language == 'en' || $language == 'am'){
                $this->lang = $language;
            }else{
                $this->lang = 'en';
            }

        }else{
            $this->lang = 'en';
        }
    }

    public function getSearchResults(Request $request){
        $lang = $this->lang;

        $term = $request->get('params');
        $limits = $this->pagination($request);
        if ($term != null) {
            $search = News::where('title_am', 'like', '%' . "$term" . '%')
                ->orWhere('title_en', 'like', '%' . "$term" . '%')
                ->orWhere('description_am', 'like', '%' . "$term" . '%')
                ->orWhere('description_en', 'like', '%' . "$term" . '%')
                ->orWhere('text_am', 'like', '%' . "$term" . '%')
                ->orWhere('text_en', 'like', '%' . "$term" . '%')
                ->limit($limits['limit'])->offset($limits['offset'])
                ->get();
        } else {
            $search = News::limit($limits['limit'])->offset($limits['offset'])
                ->get();
            $query = $request->get('query');
            $this->validate($request, [
                'type' => 'required | in:news,page',
                'query' => 'required'
            ]);
            $arr = ['lang' => $lang, 'query' => $query];
            switch ($request->type) {
                case 'news';
                    $search = News::where('title_' . $lang, 'like', '%' . "$query" . '%')
                        ->orWhere('description_' . $lang, 'like', '%' . "$query" . '%')
                        ->orWhere('text_' . $lang, 'like', '%' . "$query" . '%');
                    $count = $search->get()->count();
                    $result = $search->paginate(10);

                    break;
                case 'page';
                    $search = Page::with('menu')->has('menu')
                        ->where(function ($q) use ($arr) {
                            $q->where('title_' . $arr['lang'], 'like', '%' . $arr['query'] . '%')
                                ->orWhere('description_' . $arr['lang'], 'like', '%' . $arr['query'] . '%')
                                ->orWhere('text_' . $arr['lang'], 'like', '%' . $arr['query'] . '%');
                        });
                    $count = $search->get()->count();
                    $result = $search->paginate(10);

            }
            $result_array = [];
            if ($count > 0) {
                foreach ($result as $item) {
                    if ($request->type == 'page') {
                        $result_array[] = [
                            'id' => $item['id'],
                            'slug' => $item->menu->url,
                            'title' => $item['title_' . $lang],
                            'description' => $item['description_' . $lang],
                            'cover' => $item['cover'],
                            'ext' => $item['ext']
                        ];
                    } else {
                        $result_array[] = [
                            'id' => $item['id'],
                            'title' => $item['title_' . $lang],
                            'description' => $item['description_' . $lang],
                            'cover' => $item['cover'],
                            'ext' => $item['ext'],
                        ];
                    }
                }
            }
            return response()->json(
                [
                    'count' => $count,
                    'type' => $request->type,
                    'result' => $result_array
                ]
            );
        }
    }


    public function pagination(Request $request){

        $page = $request->header('page');
        $limit = $request->header('limit');
        if (!$limit) {
            $limit = 10;
        }
        if ($page > 1) {
            $offset = ($page * $limit) - $limit;
        } else if ($page = 1) {
            $offset = ($page * $limit) - $limit;
        }
        return ['limit' => $limit, 'offset' => $offset];
    }

    public function newsId(Request $request){
        $lang = $this->lang;
        $id = $request->id;
        $news_array = [];
        $arr =[];
        if ($id) {
            $news = News::where('id', $id)->where('title_'.$lang, "!=", null)->orWhere('description_'.$lang, "!=", null)->orWhere('text_'.$lang, "!=", null)->first();
            if ($news){

                $arr = [
                    'id' => $news['id'],
                    'title' => $news['title_'.$lang],
                    'description' => $news['description_'.$lang],
                    'text'  => $news['text_'.$lang],
                    'cover' => $news['cover'],
                    'ext'   => $news['ext'],
                    'category_id' => $news['category_id'],
                ];
            }
        } else{
            $newsAll = News::where('title_'.$lang, "!=", null)->orWhere('description_'.$lang, "!=", null)->orWhere('text_'.$lang, "!=", null)->get();
            $news =  News::where('title_'.$lang, "!=", null)->orWhere('description_'.$lang, "!=", null)->orWhere('text_'.$lang, "!=", null)->paginate(10);
            $countNews = $newsAll->count();
            foreach ($news as $item){
                $news_array[] =  [
                    'id' => $item['id'],
                    'title' => $item['title_'.$lang],
                    'description' => $item['description_'.$lang],
                    'cover' => $item['cover'],
                    'ext'   => $item['ext'],
                    'category_id' => $item['category_id'],
                ];
            }
            $arr =[
                'count' => $countNews,
                'news' => $news_array
            ];
        }
        return response()->json($arr);
    }
}
