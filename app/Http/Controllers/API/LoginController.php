<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Support\Facades\Hash;
use Validator;
//use Illuminate\Support\Carbon;

class LoginController extends Controller
{
    private $apiToken;

    public function __construct()
    {
        $this->apiToken = uniqid(base64_encode(str_random(60)));
    }


    /**
     * Create
     */
//    public function create(Request $request)
//    {
//
//        $rules = [
//            'name'     => 'required',
//            'email'    => 'required|unique:subscribers,email',
//            'password' => 'required',
//        ];
//        $validator = Validator::make($request->all(), $rules);
//        if ($validator->fails()) {
//
//            return response()->json([
//                'message' => $validator->messages(),
//            ]);
//        } else {
//            $postArray = [
//                'name'       => $request->name,
//                'email'      => $request->email,
//                'password'   => Hash::make($request->password),
//                'api_token'  => Str::random(60),
//            ];
//
//            // $user = User::GetInsertId($postArray);
//            $user = User::insert($postArray);
//
//            if($user) {
//
//                return response()->json([
//                    'name'       => $request->name,
//                    'email'      => $request->email,
//                    'password'   => $request->password,
//                    'api_token'  => Str::random(60),
//                ]);
//            } else {
//                return response()->json([
//                    'message' => 'Registration failed, please try again.',
//                ]);
//            }
//        }
//    }

    /**
     * Client Login
     */
//    public function login(Request $request)
//    {
//        $token = $request->header('Authorization');
//        $user = User::where('api_token',$token)->first();
//        if($user) {
//            return response()->json([
//                'message' => 'This is your page',
//            ]);
//        } else {
//            return response()->json([
//                'message' => 'User not found, invalid api_token',
//            ]);
//        }
//    }
}
