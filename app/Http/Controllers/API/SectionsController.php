<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Section;

class SectionsController extends Controller
{
    public function getSections()
    {
        return Section::get();
    }
}
