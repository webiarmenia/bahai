<?php

namespace App\Http\Controllers\API;

use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use App\Translation;
use App\Slider;


class PagesController extends Controller{

    public function __construct(Request $request){
        $language = $request->header('language');
        if(isset($language)) {
            if ($language == 'en' || $language == 'am'){
                $this->lang = $language;
            }else{
                $this->lang = 'en';
            }

        }else{
            $this->lang = 'en';
        }
    }

//    public function getPageId(Request $request){
//        $lang = $this->lang;
//        $this->validate($request,[
//            'id' => 'required',
//        ]);
//        $page = Page::where('id',$request->id)->first();
//        if ($page){
//            return response()->json( [
//                'title'       => $page['title_'.$lang],
//                'description' => $page['description_'.$lang],
//                'text'        => $page['text_'.$lang],
//            ]);
//        }else{
//            return response()->json(null);
//        }
//    }

    public function getPages(){
        $lang = $this->lang;
//        $pages = Page::get(['id', 'title_en','title_am','description_en','description_am','text_en','text_am']);
        $pages = Page::where('title_'.$lang, "!=", null)->orWhere('description_'.$lang, "!=", null)->orWhere('text_'.$lang, "!=", null);
        $pages_array =[];
        foreach ($pages as $page){
            array_push($pages_array,[
                'id' => $page['id'],
                'title' => $page['title_'.$lang],
                'description' => $page['description_'.$lang],
                'text' => $page['text_'.$lang],
            ]);
        }

        return response()->json($pages_array);
    }

    public function pagination(Request $request){
        if (!empty($request->page)) {
            $page = $request->page;
        } else {
            $page = 1;
        }

        if (!empty($request->perpage)) {
            $perpage = $request->perpage;
        } else {
            $perpage = 5;
        }

        $offset = ($page - 1) * $perpage;

        $pages = Page::limit(3)->offset($offset)->get();
        return $pages;
    }
    public function getLabels(){
        $lang = $this->lang;
        $labels = Translation::select('key','translation_en', 'translation_am')->get();
        $labels_array = [];
        foreach ($labels as $item){
            $labels_array[] = [
                'key' => $item['key'],
                'translation' => $item['translation_'.$lang],
            ];
        }
        return response()->json($labels_array);
    }

    public function getBanner(){
        $lang = $this->lang;
        $banner = Slider::inRandomOrder()->first();
        $banner_array = [];
        if ($banner){
            $banner_array = [
                'id' =>$banner['id'],
                'title' =>$banner['title_'.$lang],
                'subtitle' =>$banner['subtitle_'.$lang],
                'image' => $banner['image'],
                'ext'   => $banner['ext'],
            ];
        }
        return response()->json($banner_array);
    }


}
