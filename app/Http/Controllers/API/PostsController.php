<?php


namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    public function getPostsByCategory(Request $request){
        $this->validate($request, [
            'id' => 'required'
        ]);

        $posts = Post::where('category_id',$request->id)->get()->toArray();
        $posts_array = [];
        if (!empty($posts)){
            foreach ($posts as $post){
                array_push($posts_array,[
                    'id' => $post['id'],
                    'title_en' => $post['title_en'],
                    'title_am' => $post['title_am'],
                    'description_en' => $post['description_en'],
                    'description_am' => $post['description_am'],
                    'cover' => $post['cover'],
                ]);
            }
        }
        return response()->json($posts_array);
    }

    public function getCategory(Request $request){
        $this->validate($request, [
            'id' => 'required'
        ]);

        $post = Post::where('id',$request->id)->first();
        $post_array = [];
        if ($post){
            $post_array = [
                    'id' => $post['id'],
                    'title_en' => $post['title_en'],
                    'title_am' => $post['title_am'],
                    'description_en' => $post['description_en'],
                    'description_am' => $post['description_am'],
                    'cover' => $post['cover'],
             ];
        }
        return response()->json($post_array);
    }

}