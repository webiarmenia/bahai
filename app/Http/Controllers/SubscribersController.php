<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;

class SubscribersController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $subscribers = Subscriber::orderBy('id','DESC')->paginate(10);
        return view('admin.subscribers.index')->with([
            'subscribers' => $subscribers,
        ]);
    }

    public function subscriberChange(Request $request,$id){
        $subscriber = Subscriber::where('id',$id)->firstOrfail();
        if($subscriber->active == true){
            $activeValue = false;
        }else{
            $activeValue = true;
        }
        Subscriber::where('id',$id)->update([
            'active' => $activeValue
            ]
        );
        return back();
    }

    public function subscriberDestroy($id){
        $subscriber = Subscriber::where('id',$id)->firstOrfail();
        if($subscriber) {
            $subscriber->delete();
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteAll(Request $request){
        $subscriber = Subscriber::whereIn('id',$request->get('arr'))->delete();
    }
}
