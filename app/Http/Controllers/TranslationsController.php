<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Translation;

class TranslationsController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $translations = Translation::orderBy('id', 'desc')->paginate(10);
        return view('admin.translation.index', compact('translations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin.translation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $request->validate([
            'translation_en' =>'required',
            'key' =>'required'
        ]);
        $translations = new Translation([
            'translation_en'=> $request->get('translation_en'),
            'translation_am'=> $request->get('translation_am'),
            'key'=> $request->get('key')
        ]);
        $translations->save();
        return redirect()->route('admin.translations')->with('success', 'Translations created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $translations = Translation::where('id',$id)->firstOrfail();
        return view('admin.translation.edit')->with([
            'translations' => $translations,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate([
            'translation_en'=> 'required',
            'key'=> 'required',
        ]);
        $translations = Translation::where('id',$id)->firstOrfail();
        $translations->translation_am = $request->get('translation_am');
        $translations->translation_EN = $request->get('translation_en');
        $translations->key = $request->get('key');
        $translations->save();
        return redirect()->route('admin.translations')->with('success', 'Translations was updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){
        $translations = Translation::where('id',$id)->firstOrfail();
        $translations->delete();
        return redirect()->route('admin.translations')->with('success', 'Translations successfully deleted');
    }
    public function deleteAll(Request $request){
        $translations = Translation::whereIn('id',$request->get('arr'))->delete();
    }
}
