<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model{
    protected $fillable = [
        'title_am',
        'title_en',
        'description_am',
        'description_en',
        'text_am',
        'text_en'
    ];

    public function menu(){
        return $this->hasOne('App\Menus','page_id','id');
    }
}
