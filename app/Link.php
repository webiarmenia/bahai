<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model{
    protected $fillable = [
        'text_am',
        'text_en',
        'link',
        'image',
        'page_id',
        'ext'
    ];
}
