<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model{
    protected $fillable = [
        'title_am',
        'title_en',
        'description_am',
        'description_en',
        'text_am',
        'text_en'
    ];


}
