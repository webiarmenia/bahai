<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $message;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$message,$link=null)
    {
        //

        $this->subject = $subject;
        $this->message = $message;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $e_subject = $this->subject;
        $e_message = $this->message;
        $e_link = $this->link;
        return $this->view('emails.sendEmail',compact("e_message","e_subject","e_link"))->subject($e_subject);
    }
}
